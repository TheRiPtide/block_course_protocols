\documentclass[11pt,a4paper]{article}

\usepackage
[
a4paper,
left=2.5cm,
right=3cm,
top=3cm,
bottom=4cm
]{geometry}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{multicol}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{parskip}
\usepackage[english]{babel}
\usepackage{cite}
\usepackage[numbib,nottoc]{tocbibind}
\usepackage{bbm}
\usepackage{epsfig}
\usepackage{booktabs}
\usepackage{float}
\usepackage{caption}
\usepackage{siunitx}
\usepackage{longtable}
\usepackage{pdfpages}
\usepackage{caption}
\captionsetup[figure]{font=small}
\usepackage{chemformula}
\setchemformula{circled=all}
\setchemformula{circletype=chem}
\usepackage{adjustbox}
\usepackage[hidelinks]{hyperref}

\sisetup{
	round-mode          = places, % Rounds numbers
	round-precision     = 3, % to 3 places
}


\setlength{\parindent}{0pt}
\newcommand{\degs}{\ensuremath{^\circ}}

\author{Gregory Zaugg}
\title{Microscopy}

\date{\today}

\begin{document}
	\onehalfspacing	
	\pagenumbering{arabic}
	\setlength\abovedisplayskip{10 pt}
	\setlength\belowdisplayskip{10 pt}
	\righthyphenmin 62
	\lefthyphenmin 62
	
\begin{titlepage}
	\begin{minipage}{.47\textwidth}
		\begin{flushleft}
			\includegraphics[width=3cm]{resources/SNI_black_on_white}
		\end{flushleft}
	\end{minipage}
	\hskip .2\textwidth
	\begin{minipage}{.45\textwidth}
		
		\includegraphics[width=5cm]{resources/UniBas_Logo_EN_Schwarz_RGB_65.pdf}
		
	\end{minipage}
	\begin{center}
		\vskip 1.5cm
		\bfseries
		\large Nanoscience block course\\
		\hrulefill \\		
		\vspace{0.5 cm}		
		\textsc{\huge Cleaning of S-2 Glass Fiber Sizing} 
		\\ \hrulefill
		\vskip 1.5cm
		\mdseries
		\large 
		{
			\begin{tabular}{rl}
				        \textbf{Author:} & \underline{Gregory Zaugg}     \\
				\textbf{Date of course:} & {15. - 26.02.2021}            \\
				    \textbf{Supervisor:} & Thomas A. Jung, Mehdi Heydari
			\end{tabular}
		}
		\bfseries
		
	\end{center}
	
	\vskip 2cm	
	
	\begin{figure}[H]
		\centering
		\captionsetup{justification=centering,margin=1cm}
		\includegraphics[width=0.8\linewidth]{graphs/cern_Nb3Sn_Wire}\\
		\ch{Nb3Sn} cable, showing the single strands and the glass-fiber insulation, 
		partially unwrapped\cite{TamingSuperconductorsTomorrow2020}.
		\label{fig:cernnb3snwire}
	\end{figure}
		
		
\end{titlepage}

\tableofcontents

\newpage

\section{Introduction}

Glass Fiber Reinforced Polymer (GFRP) composites have been among the most commonly used composite materials in engineering due to their broad range of applications. They have found their use in electronics as backbones for circuit boards, aviation for adding structural stability in wings and hull, automobile application such as chassis and many more. This is mostly due to the excellent properties of the Glass Fibers (GFs) like high strength, flexibility, low weight and resistance to chemical harm \cite{sathishkumarGlassFiberreinforcedPolymer2014}. One of the applications is structural support for brittle or soft materials. In this case we are studying GFRP composites as a structural support for brittle Niobium-Tin (\ch{Nb3Sn}) based superconducting Rutherford cables.

  \ch{Nb3Sn} is a type II superconductor and is commonly used in applications such as di- and quadrupole magnets in particle accelerators (such as the LHC \cite{TamingSuperconductorsTomorrow2020}) and for toroidal field coils in tokamak fusion devices \cite{xuReviewProspectsNb3Sn2017} (such as ITER \cite{FactsFigures}). But due to its brittle nature it needs to be stabilized or else it can easily crack. Cracks form interfaces detrimental to the materials superconductive properties. This is where GFs and GFRPs come in handy, as the GFs can survive the high temperatures that are required in the fabrication process of \ch{Nb3Sn} based superconductors and by coating them in Epoxy a strong rigid shell can be formed that adds the structural integrity of the superconducting cable.

Conventionally the GF braids that are usually sold by suppliers are coated in various sizings. These help protect the GFs during manufacturing and aid epoxies in binding to the surface. As such these coatings are usually designed in a fashion similar to amphiphiles, with one end (usually a silane) binding to the Si Surface of the GFs and an exposed residue (an epoxide or amine) for the epoxy to bind (see figure \ref{gammaAPSGPS}).

\begin{figure}[H]
	\centering
	\begin{tabular}{c c}
		\includegraphics[width=52mm]{graphs/gamma-APS} &
		\includegraphics[width=65mm]{graphs/gamma-GPS}
	\end{tabular}
	\caption{(LTR) $\gamma$-APS and $\gamma$-GPS, two commonly used molecules exhibiting the aforementioned properties.}
	\label{gammaAPSGPS}
\end{figure}

This is a problem since the fiber-wrapped Rutherford cable needs to be heat-treated (650 \degs C, 75 h) in order for the brittle, superconducting Niobium-Tin to form. During this treatment the sizing pyrolizes and leaves carbon residues on the GF braids and nearby surfaces.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{graphs/pyrolized_GF_sizing}
	\caption{Carbon residues due to pyrolized sizing on the GF braids.}
	\label{fig:pyrolizedgfsizing}
\end{figure}

The goal of this project will be to find a reproducible procedure to clean the sizing from the glass braids. X-ray photo-electron spectroscopy (XPS) will be used to quantify the efficiency and thoroughness of the cleaning procedure, as well as to identify the rough composition of the sizing itself.


\section{Materials \& Methods}

The material that was analyzed is S-2 glass fiber braids. S-2 glass is a product by the AGY Holding Corp. The material itself is S-Glass, a type of glass produced for its superior mechanical properties due to its high aluminum content \cite{bunsellHighperformanceFibers2005}. The analysis was conducted in the Ultra-High Vacuum (UHV) lab of Thomas Jung at the Paul Scherrer Institute (PSI).

XPS works by irradiating the surface of a material using X-rays and then measuring the kinetic energy of the ejected electrons through the photoelectric effect. This gives information on the binding energy of the electrons and therefore on the elemental composition as well as the chemical state of said elements. 

The XPS analysis was done using an monochromated Al K$\alpha$ X-ray source giving a photon energy of 1486.7 eV according to Nist X-ray Transition Energies Database \cite{XrayTransitionEnergies}. We used an electron flood gun to compensate for charging effects in the samples as S-Glass is an insulator and thus quickly becomes positively charged. This causes large shifts of the peaks in the spectrum to higher binding energies \cite{manjuElectricalConductivityStudies2018} \cite{ChargeCompensationSample}.

The Software used to control the XPS was SpecsLab Version 2.83.

In order to give a semi-quantitative result on different cleaning procedures a reference had to  be formed. For this purpose two differently cleaned samples were prepared as well as a reference. 

	\subsubsection{Sample Preparation}
	
	As a pretreatment step every sample was washed in technical acetone for 1 minute in a sonication bath, then rinsed twice with acetone and dried in an oven at 60 \degs C. This was used as a reference for the analysis.
	
	Sample 1 was then further washed in a 1 wt.\% solution of Alconox (an industrial glass cleaner) at 40 \degs C for 10 minutes in a sonication bath. Finally it was rinsed twice with DI water and dried in an oven at 60 \degs C.
	
	As for sample 2 it was exposed to hot air from a heat gun for 5 minutes at approximately 300 \degs C.

	\subsubsection{Measurement}
	
	A broad survey from 900 to 0 eV was done for each sample to identify the different visible peaks (see figure \ref{fig:survey}). Each of those was then scanned again in a smaller range with averaging over 5 measurements to improve the signal-to-noise ratio. Additionally all the expected peaks, according to table \ref{tab:comp}, that were not visible in the survey were scanned with the same method, in order to confirm either their presence or absence. Furthermore, a negative control for boron was made to ensure that the material was S-Glass.
	
	\vspace{1 cm}
	
	\begin{table}[h!]
		\begin{center}
			\begin{tabular}{|c|c|c|c|}
				\hline
				\textbf{Element} & \ch{SiO2} & \ch{Al2O3} & \ch{MgO} \\ \hline
				\textbf{wt.\%}  &    65     &     25     &    10    \\ \hline
			\end{tabular}
			\caption{Composition of S-Glass \cite{bunsellHighperformanceFibers2005}.}
			\label{tab:comp}
		\end{center}
	\end{table}


\section{Results}
	
	In figure \ref{fig:survey} one can directly identify four peaks: From left to right the large oxygen peak at $\sim$ 550 eV, the carbon peak at $\sim$ 280 eV (best visible in the reference sample) and the two silicon peaks around 100 eV (best visible in sample 1). The spectra were shifted by -1.5, -3.5 and -2.5 eV for the reference, sample 1 and sample 2 respectively to account for the shift due to the flood gun usage. These values were estimated in regard to the silicon peak as it remained mostly constant and had enough signal strength to make a statement of sufficient certainty.
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=\linewidth]{data/survey}
		\caption{Survey scans of all three samples from 900 to 0 eV.\\ 
		a) Reference\\
		b) Sample 1\\
		c) Sample 2}
		\label{fig:survey}
	\end{figure}

	Figure \ref{fig:surveyoverlay} shows the three surveys normalized according to the last and first 50 points and smoothed using a 3rd order polynomial Savitzky-Golay filter for the nearest 40 points. Even from this one can notice a significant reduction in carbon signaling and an increase in both silicon as well as oxygen for both sample 1 and 2 when comparing to the reference.
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=1\linewidth]{data/survey_overlay}
		\caption{All three surveys overlaid. Normalized according to the mean of the first and last 50 points, smoothed using 3rd order polynomial Savitzky-Golay filter for 40 nearest points.}
		\label{fig:surveyoverlay}
	\end{figure}
	

	\newpage
	
	\subsection{Oxygen 1s}
	
	\begin{multicols}{2}
		
		Figure \ref{fig:o1s} shows the oxygen 1s peak in the range or 540 $\sim$ 525 eV for a) the reference, b) sample 1 and c) sample 2, averaged over 5 cycles to reduce noise .\\
		\\
		As the survey spectra indicated an absence of Nitrogen, we expect the coating to be epoxy-based. Therefore for the reference in figure \ref{fig:o1s} a), taking the structure of $\gamma$-GPS into account, fitting for three oxygen peaks is the most sensible. When comparing to reference material we suspect peak 1 to be the carbon-bound oxygen, peak two to be the \ch{SiO2} and peak three to be \ch{Al2O3} (see figure \ref{fig:o1s} a)). \\
		\\
		Looking at sample 1 only two peaks remain, when comparing to the previous spectrum they seem to be the \ch{SiO2} and \ch{Al2O3} peaks. The C-O peak seems to have disappeared or at least gotten small enough to be indistinguishable from the \ch{SiO2} peak (see figure \ref{fig:o1s} b)). \\
		\\
		As for sample 2 in figure \ref{fig:o1s} c) there seems to be a slight peak for carbon-bound oxygen, though it is greatly reduced. The other two peaks appear to be roughly the same as in the reference.
		
		\columnbreak
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=1\linewidth]{data/plots/o1s}
			\caption{Oxygen 1s peaks in the spectra averaged over 5 cycles.\\ 
				a) Reference\\
				b) Sample 1\\
				c) Sample 2
				}
			\label{fig:o1s}
		\end{figure}
		
		
	\end{multicols}
	
		


	\newpage
	
	\subsection{Carbon 1s}
	\begin{multicols}{2}
		In figure \ref{fig:c1s} the carbon 1s peak in the range of 295 $\sim$ 280 eV is shown for a) the reference, b) sample 1 and c) sample 2, again averaged over 5 cycles.\\
		\\
		Regarding carbon, fitting for three peaks also gives the most sensible results (see figure \ref{fig:c1s} a))). When looking at the reference peak 2 is most likely an overlap of C-C and C-O bonds \cite{NISTXPSDatabase}. Peak 3 seems to be the C-Si bonds from the silane \cite{NISTXPSDatabase}. Peak 1 looks to be the carboxyl groups from fatty acid residues in lubricants that are used when braiding the GFs.\\
		\\
		For sample 1, the carbon peak has the most significant reduction in intensity. First of all, peak 1 stemming from the carboxyl groups of fatty acids seems to have completely disappeared. Furthermore, peak 2 representing C-C and C-O bonds is significantly smaller. Only peak 3, the silicon-bound carbon, is still close to the same, if not more intense (see figure \ref{fig:c1s} b)).\\
		\\
		The same can be said for sample 2. However there is a strange 3rd peak that only appeared here and we could not assign with sufficient certainty. It is most likely some form of contamination.
		
		\columnbreak
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=1\linewidth]{data/plots/c1s}
			\caption{Carbon 1s peaks in the spectra averaged over 5 cycles.\\ 
				a) Reference\\
				b) Sample 1\\
				c) Sample 2
			}
			\label{fig:c1s}
		\end{figure}
	\end{multicols}
	
	\newpage
	
	\subsection{Silicon 2p}
	\begin{multicols}{2}
		Figure \ref {fig:si2p} presents the silicon 2p peaks in the range of 110 $\sim$ 95 eV for a) the reference, b) sample 1 and c) sample 2, averaged over 5 cycles.\\
		\\
		As for silicon, there also seem to be three peaks. When comparing to the reference, peak 1 matches well with what we would expect for \ch{SiO2} and peak 2 looks to be the silane \cite{dietrichSynchrotronradiationXPSAnalysis2016}. Peak 3 is what we would expect for Si-Si or the Si-C bonds in the silane. Since in extruded glass Si$^0$ is not to be expected, Si-C seems to be more likely.\\
		\\
		Peak 1 (\ch{SiO2}) in the silicon signal of sample 1 (see figure \ref{fig:si2p} b)) seems to have significantly increased in intensity. Peak 2 (silane) in comparison is a lot smaller and peak 3 (Si-C) seems to have completely disappeared.\\
		\\
		The same can be said for sample 2. The difference between the two samples here is not noticeable.
		
		\columnbreak
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=1\linewidth]{data/plots/si2p}
			\caption{Silicon 2p peaks in the spectra averaged over 5 cycles.\\ 
				a) Reference\\
				b) Sample 1\\
				c) Sample 2
			}
			\label{fig:si2p}
		\end{figure}
		
	\end{multicols}
	
	\newpage
	
	\subsection{Aluminum 2p}
	\begin{multicols}{2}
		Figure \ref{fig:al2p} shows the aluminum 2p peak between 80 $\sim$ 70 eV for a) the reference, b) sample 1 and c) sample 3, averaged over 5 cycles.\\
		\\
		As for aluminum there is a very weak peak. Given the contents of S-Glass seen in table \ref{tab:comp} and checking with the database this seems to be the \ch{Al2O3} \cite{NISTXPSDatabase}.\\
		\\
		For sample 1 the signal seems to have significantly increased and a second peak has appeared (see figure \ref{fig:al2p} b)). This seems to be a different species of oxide which was indistinguishable before due to the low signal-to-noise ratio.\\ 
		\\		
		For sample 2 we again have a very low signal, so fitting for multiple peaks is not really sensible. Still the peak is more prominent than in the reference.
		
		\columnbreak
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=1\linewidth]{data/plots/al2p}
			\caption{Aluminum 2p peaks in the spectra averaged over 5 cycles.\\ 
				a) Reference\\
				b) Sample 1\\
				c) Sample 2
			}
			\label{fig:al2p}
		\end{figure}

	\end{multicols}


\section{Discussion and Conclusion}

We have investigated three different samples using XPS analysis in an ultra-high-vacuum (UHV) condition. The reference sample gave us insight into the composition of the coating, as without cleaning procedures most of the recorded signal stemmed from the sizings. Since only carbon and oxygen peaks were recorded apart from the very weak silicon and aluminum peaks from the underlying glass, the sizing is organic and likely epoxy-based. 

Regarding sample 1 we could see a significant increase in oxygen signaling as well as a decrease in carbon signaling. Additionally both silicon and aluminum showed higher signal. We take this as evidence that a good portion of the coatings were removed.
As for sample 2, the carbon peak has significantly reduced in signaling and the oxygen, silicon as well as aluminum have increased. However some strange peaks appeared that could not be assigned with sufficient certainty. This is likely a form of contamination from the cleaning procedure. 

As for the difference between the methods, it is very hard to say. While sample 1 had a bigger increase in oxygen signaling, sample 2 had a higher reduction for carbon. So depending on the goal, the second method seems to be slightly more effective. But his means that at least to some extent the procedures were successful. Since XPS analysis is only semi-quantitative we cannot say for certain exactly how effective they were. A potential solution I would propose is to get a second reference where a very aggressive cleaning method is used that would not be sensible for industrial production but could provide a baseline for how a nearly perfectly clean sample would appear in the XPS. One option for this would be a so called "piranha solution" which is a strong oxidizing agent that can remove most organic matter\cite{Piranha2010}. 

To conclude, we observed a cleaning effect for both treatments and found ways that could improve the results of further analysis. Further, this blockcourse gave a very interesting insight into UHV lab practices and real practiced science. Finally I was able to learn about XPS analysis, which I had no prior knowledge of.


	
\section{Acknowledgments}

Fist of all I would like to thank Prof. Dr. Thomas A. Jung for providing me with this interesting project to work on and access to his lab. Further a big thanks goes to Mehdi Heydari for supervising us throughout these two weeks. Finally I would like to thank the SNI for organizing this blockcourse.

\newpage

\bibliographystyle{IEEETRAN}
\bibliography{Bibliography}
	
\end{document}