import glob
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.signal import savgol_filter

"""
Shift for each sample: 

ref: 1.5
s1 : 3
s2 : 2.5
"""

labels = {0: 'a)',
          1: 'b)',
          2: 'c)'}

shifts = {'ref': 1.5,
          's1': 3.5,
          's2': 2.5}


def plot_all(show):
    dir = './python_data/'

    peaks = ['o1s', 'c1s', 'al2p', 'si2p']

    for peak in peaks:
        files = glob.glob(dir + f'*_{peak}.txt')

        x_axis = []
        cps = []
        fig, ax = plt.subplots(3, 1, figsize=(6.4, 15))

        for i, path in enumerate(files):

            file = Path(path)

            name = file.stem

            sample = name.split('_')[0]

            df = pd.read_csv(file, sep='\t', decimal=',', skiprows=3)

            x = df.iloc[:, 1]

            x_axis = np.subtract(x, shifts[sample])

            cps = (df.iloc[:, 2])

            ax[i].plot(x_axis, cps, '.', color='grey', label=cps.name)
            ax[i].set_xlabel('Binding Energy [eV]')
            ax[i].set_ylabel('Intensity [cps]')

            for idx in df.columns[3:]:
                ax[i].plot(x_axis, df[idx], '-', label=idx.rsplit('_', 1)[0])

            ax[i].invert_xaxis()

            ax[i].text(0.05, 0.9, labels[i], fontsize=14, transform=ax[i].transAxes)

            ax[i].legend()

        plt.tight_layout(pad=3.0)

        plt.subplots_adjust(hspace=0.3)

        plt.savefig(f'plots/{peak}.png')
        if show: plt.show()
        plt.cla()

    print('plotted all')


def plot_suvey(show):
    files = glob.glob('./python_data/*_survey.txt')
    dfs = []

    sample_names = ['Ref', 'S1', 'S2']
    sample = []

    for i in files:
        file = Path(i)
        name = file.name
        loc = file.parent

        sample.append(name.split('_')[0])

        dfs.append(pd.read_csv(file, sep='\t', decimal=',', skiprows=3))

    fig, ax = plt.subplots(3, figsize=(10, 7), sharex=True)
    plt.xlabel('Binding Energy [eV]')

    smoothed_normed_cps = []
    x_vals = []

    for i, df in enumerate(dfs):
        x_axis = df.iloc[:, 1] - shifts[sample[i]]
        cps = df.iloc[:, 2]

        ax[i].set_ylabel(f'{sample_names[i]} Intensity [cps]')

        ax[i].plot(x_axis, cps, '.', color='grey', label=cps.name)

        stop_avg = np.mean(cps[-50:])

        zeroed_cps = cps - stop_avg

        start_avg = np.mean(zeroed_cps[:50])

        normed_cps = zeroed_cps / start_avg

        smoothed_normed_cps.append(savgol_filter(normed_cps, 41, 3))
        x_vals.append(x_axis)

        ax[i].text(0.03, 0.82, labels[i], fontsize=14, transform=ax[i].transAxes)

    plt.tight_layout()

    # plt.legend()
    plt.gca().invert_xaxis()
    plt.savefig(f'survey.png')
    if show: plt.show()
    plt.cla()

    plt.figure(figsize=(10, 4))

    plt.ylabel('Normalized Intensity [arb]')
    plt.xlabel('Binding Energy [eV]')

    alp = 0.6

    plt.plot(x_vals[0], smoothed_normed_cps[0], label='Ref', alpha=alp)
    plt.plot(x_vals[1], smoothed_normed_cps[1], label='S1', alpha=alp)
    plt.plot(x_vals[2], smoothed_normed_cps[2], label='S2', alpha=alp)

    plt.legend()

    plt.gca().invert_xaxis()

    plt.tight_layout()

    plt.savefig('survey_overlay.png')
    if show: plt.show()

    print('surveys plotted')


# def special_plot(show):
#     file = Path(f'python_data/ref_c1s.txt')
#     name = file.stem
#     loc = file.parent
#
#     df = pd.read_csv(file, sep='\t', decimal=',', skiprows=0)
#
#     x_axis = df.iloc[:, 0]
#
#     x_axis = np.subtract(x_axis, shifts['ref'])
#
#     cps = df.iloc[:, 1]
#
#     plt.plot(x_axis, cps, '.', color='grey', label=cps.name)
#     plt.xlabel('Binding Energy [eV]')
#     plt.ylabel('Intensity [cps]')
#     plt.tight_layout()
#
#     for i in df.columns[2:]:
#         plt.plot(x_axis, df[i], '-', label=i.rsplit('_', 1)[0])
#
#     plt.legend()
#     plt.gca().invert_xaxis()
#     plt.savefig(f'{loc}/{name}.png')
#     if show: plt.show()

    print('special done')


if __name__ == '__main__':
    show = True

    plot_all(show)
    # special_plot(show)
    # plot_suvey(show)
