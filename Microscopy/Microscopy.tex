\documentclass[11pt,a4paper]{article}

\usepackage
[
a4paper,
left=2.5cm,
right=3cm,
top=3cm,
bottom=4cm
]{geometry}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{parskip}
\usepackage[english]{babel}
\usepackage{cite}
\usepackage[numbib,nottoc]{tocbibind}
\usepackage{bbm}
\usepackage{epsfig}
\usepackage{booktabs}
\usepackage{float}
\usepackage{caption}
\usepackage{siunitx}
\usepackage{longtable}
\usepackage{pdfpages}
\usepackage{caption}
\captionsetup[figure]{font=small}
\usepackage{chemformula}
\setchemformula{circled=all}
\setchemformula{circletype=chem}
\usepackage[hidelinks]{hyperref}

\sisetup{
	round-mode          = places, % Rounds numbers
	round-precision     = 3, % to 3 places
}


\setlength{\parindent}{0pt}
\newcommand{\degs}{\ensuremath{^\circ}}

\author{Gregory Zaugg}
\title{Microscopy}

\date{\today}

\begin{document}
	\onehalfspacing	
	\pagenumbering{arabic}
	\setlength\abovedisplayskip{2 pt}
	\setlength\belowdisplayskip{10 pt}
	\righthyphenmin 62
	\lefthyphenmin 62
	
\begin{titlepage}
	\begin{minipage}{.45\textwidth}
		\begin{flushleft}
			\includegraphics[width=3cm]{graphs/SNI_black_on_white}
		\end{flushleft}
	\end{minipage}
	\hskip.4\textwidth
	\begin{minipage}{.45\textwidth}
		
		\includegraphics[width=2cm]{graphs/Uni_basel_logo.svg.png}
		
	\end{minipage}
	\begin{center}
		\vskip 1.5cm
		\bfseries
		\large Nanoscience block course\\
		\hrulefill \\		
		\vspace{0.5 cm}		
		\textsc{\huge Microscopy}
		\\ \hrulefill
		\vskip 1.5cm
		\mdseries
		\large 
		{
			\begin{tabular}{rl}
				        \textbf{Author:} & Gregory Zaugg                                   \\
				\textbf{Date of course:} & {26.10. - 13.11.2020}                           \\
				   \textbf{Supervisors:} & Evi Bieler, Susanne Erpel, Markus Dürrenberger, \\
				                         & Daniel Mathys, Monica Schönenberger
			\end{tabular}
		}
		\bfseries
		
	\end{center}
	
	\vskip 1.5cm		
		
\end{titlepage}

\tableofcontents
	
\newpage

\section{Introduction}

As a student in the field of nanoscience, it is very rare that my current or future work will involve something visible to the human eye without any supporting technology. This is why Microscopy is a skill that belongs on every nanoscience student's tool-belt. The word Microscopy stems from the Ancient Greek words mikrós ("small") and skopéō ("I look at"), meaning the visualization of something small. And as the objects to visualize get smaller, light interaction becomes insufficient and a different imaging technique other than Optical Microscopy (OM) is needed. For the past decades the Scanning Electron Microscope (SEM) has proven to be invaluable for visualizing at a scale where OM no longer suffices. 
\vskip 0.5cm
But knowing about it is far from enough, that is why this practical course aimed at giving hands-on experience on how to operate an SEM, what characteristics and peculiarities one has to be weary of and of course what peripheral knowledge is necessary to even prepare a usable specimen.
\vskip 0.5cm
All of this was sadly cut short by the rapidly approaching second wave of the current global pandemic. That is why the data in this report is was provided, and the sample preparation methods were only demonstrated though pre-recorded videos. Nevertheless, this report will give a detailed overview as to what we learned despite these complications and give a closer insight into several types of plant pests. This report will discuss the advantages and disadvantages of each sample preparation method, the challenges when imaging biological samples and the differences between SEM and OM.
	
\newpage
	
\section{Theory}
	
	\subsection{Scanning Electron Microscope (SEM)}
	
	As the name suggests the SEM is a microscope that uses electrons to scan something. The construction of such a device can be categorized in three main components: The electron gun column, the sample holding chamber and the detectors. These are themselves composed of individual parts. In chronological order we start at the electron gun, specifically the cathode. As can be seen in figure \ref{fig:schemesem} it is located at the very top of the electron gun column. It contains an electron emitter.
	
	The anode then accelerates the emitted electrons with an acceleration voltage of around 1 $\sim$ 30 kV. This produces the electron beam. Following are two magnetic condenser lenses, that condense and focus the beam. The Deflections coils right below that are responsible for aiming the beam and producing the scanning motion. 

	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{graphs/Scheme_SEM}
		\caption{Labelled Schematic of an SEM\cite{SEM_pres}.}
		\label{fig:schemesem}
	\end{figure}

	At this point the beam enters the sample holding chamber. As it hits the specimen it interacts differently with certain parts of the sample. Those different interactions are what is detected by the various detectors that exist. We will now take a closer look at three of those detectors.
	
	Inside both the sample holding chamber and the electron gun column a relatively high vacuum is applied. Within the gun, $10^{-7}$Pa are produced while the sample holding chamber is at $10^{-3}$Pa.
	
	%Aufbau und Funktionsweise, Abb
	
	\subsubsection{Secondary Electrons (SE)}
	
	SE are ionization products that stem from the very surface of the specimen, as can be seen from figure \ref{fig:specimeninteraction}.
	Primary (incident) electrons that exceed the ionization potential are able to free SEs, which are then detected. They give spatial information and are therefore used to image the topography of a sample. The detector is usually located slightly above the sample-holder to the side.
	
	%Entstehung und Information
	
	\subsubsection{Back-Scattered Electrons (BSE)}
	
	BSE are primary electrons that are reflected/back-scattered though elastic scattering interactions with the specimen atomic nuclei. Atoms with a high atomic number back-scatter more strongly they appear lighter in the resulting image, allowing detection of the contrast between different chemical compositions at a medium penetration depth \cite{goldstein2017scanning}. This detector is  located in a ring-shape around the exit point of the electron gun directly above the sample-holder.
	
	%siehe 2.2.1
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.7\linewidth]{graphs/electron-microscopy}
		\caption{Effects produced by electron beam interaction with a specimen\cite{freeskill.pk_2020}.}
		\label{fig:specimeninteraction}
	\end{figure}

	\newpage
	
	\subsubsection{Energy-Dispersive X-Ray Spectroscopy (EDX)}
	
	As the incident beam penetrates deeper into the specimen, characteristic X-rays are produced (see figure \ref{fig:specimeninteraction}). These X-rays are the result of outer-shell electrons of specimen atoms that fall into a lower shell, emitting quantized photons with energy equivalent to the energy difference between the higher and lower state. As the energy levels are unique for each element, the X-rays have frequencies that are characteristic too. As figure \ref{fig:specimeninteraction} indicates one has to take into account that these characteristic X-rays stem from a variety of depths, so it gives the composition information of not just the surface but a slice of the specimen. This detector is located in a similar fashion to the SE detector.
	
	%siehe 2.2.1, Beschreibung EDX, Abb. Anregungsbirne
	
\section{Materials and Methods}

	\subsection{Sample preparation for SEM}
	
	The environment inside an SEM is extremely harmful to biological specimens. The high vacuum causes water to evaporate which lets microstructures to crumble under the forces of surface tension. Charge build-up of the high intensity electron beam hitting a non-conductive sample can cause image distortion and even damage to a specimen. Several methods of sample preparation have been discovered that minimize or fully negate these effects.
	Since in our case, plant pests were observed, the specimen prepared with the following methods was a 5mm x 5mm cut-out of the plant that was extracted from a leaf with a scalpel.
	
	\subsubsection{Critical Point Drying (CPD)}
	
	This method utilizes the properties of supercritical liquid \ch{CO2}. The sample was placed in a specimen holder, with coverslips and distance rings to protect it.
	To stop any biological processes the specimen was first fixed with a 4\% glutaraldehyde solution. After flushing with water, it was dehydrated with an EtOH series from 20\% to 100\% in steps of 20 for 10 minutes each\cite{vid_prep}. At this point the sample was paced in a special chamber to undergo the actual CPD. First the alcohol was flushed and replaced with liquid \ch{CO2} at around 100 bar room temperature (well within the liquid zone, see figure \ref{fig:supercriticalfluid}). While keeping a constant pressure, the temperature was raised and the liquid \ch{CO2} turned supercritical. At this point the separation between gaseous and liquid phase was no longer existent. By keeping the temperature constant and slowly venting the \ch{CO2} all the previously liquid phase had now been turned gaseous without ever subjecting the specimen to capillary forces. The sample was now dry with minimal to no damage to its structure. Finally the sample is made conductive through sputtering as described in section 3.1.3.
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.6\linewidth]{graphs/supercritical_fluid}
		\caption{Carbon dioxide pressure-temperature phase diagram\cite{supercritical}.}
		\label{fig:supercriticalfluid}
	\end{figure}
	
	%vom lebenden Objekt bis zum fertigen Präparat
	
	\subsubsection{Cryo preparation}
	
	This method exploits the fact that water can form vitreous/ amorphous ice. When cooling at around  $10^{-4} \frac{K}{s}$ water is unable to form a crystal lattice which could damage the structure of the specimen. Instead the water freezes in its disorderly state. This is done by plunging the sample into nitrogen "slush" at approximately -210K. The "slushy" consistency inhibits immediate vapour formation at the surface of the specimen which would be thermally insulating.
	
	The specimen was glued to a SEM sample holding stick and plunged into nitrogen as described. It was then inserted into the Cryo-SEM under high vacuum. In a separate chamber a 30nm layer of gold is evaporated over the surface, again using the method described in section 3.1.3, in order to ensure conductivity.
	
	%siehe 3.1.1
	
	\subsubsection{Sputtering}
	
	Sputtering is a form of Physical Vapour Deposition (PVD). To prevent charge build-up especially in generally non-conductive biological samples, the electrons from the beam need to be able to drain away. This is usually done by coating a thin metallic layer over the entire sample. 
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.7\linewidth]{graphs/sputtering}
		\caption{Schematic of a sputtering setup \cite{sputter_img}.}
		\label{fig:sputtering}
	\end{figure}

	As indicated in figure \ref{fig:sputtering} Argon plasma is accelerated towards a sputtering target. The impact of each Argon Ion can cause multiple target atoms to be ejected if it has sufficient kinetic energy. Argon is used as it is a noble gas and doesn't interact with the substrate. Over time a thin layer of the target atoms starts building up on the substrate surface. 
	
	In our case we used a gold target and deposited around 30nm on top of our samples.

	%Sinn&Zweck, Prinzip
	
\newpage

\section{Results and Discussion}

	\subsection{OM- and SEM-Images of Wheat Leaf Rust}
	
	Wheat leaf rust, also known as brown rust, is a fungal disease that commonly affects wheat. Optically it appears as spots of rust-coloured particles (see figure \ref{fig:braunrost-auf-weizen}). The colouration stems from the urediniospores (or uredospores) which are spherical spores of around 20$\mu$m diameter that are covered in tiny spikes\cite{alexopoulos1996introductory}\cite{adipdf}. 
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/LM/Braunrost auf Weizen"}
		\caption{Optical Microscope (OM) image of brown rust on a wheat leaf.}
		\label{fig:braunrost-auf-weizen}
	\end{figure}

	To compare the two preparation methods we picked two images depicting a bulk of spores.
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Braunrost CPD/213242"}
		\caption{SEM image of brown rust prepared with CPD.}
		\label{fig:br-cpd}
	\end{figure}
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Braunrost Kryo/212937"}
		\caption{SEM image of brown rust based on cryo-preparation.}
		\label{fig:br-cryo}
	\end{figure}
	
	The cryo-prepared spores all seem to have a slightly deflated/blocky look compared to the CPD prepared spherical spores (figure \ref{fig:br-cryo} vs \ref{fig:br-cpd}). It is unclear however if this is a result of the preparation method or if this was present before. The consistency however indicates that it might be a result of cryo-preparation.
	
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Braunrost Kryo/212925"}
		\caption{More deflated but also intact cryo-prepared spores.}
		\label{fig:br-cryo2}
	\end{figure}

	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Braunrost Kryo/212979"}
		\caption{Closer look at a fully deflated cryo-prepared spore.}
		\label{fig:br-cryo3}
	\end{figure}

	As can be seen in figure \ref{fig:br-cryo2}, there are also intact and spherical spores after cryo-preparation but the majority is deflated. A close up of a fully deflated can also be seen in figure \ref{fig:br-cryo3}. 
	
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Braunrost CPD/213243"}
		\caption{Intact spore prepared with CPD.}
		\label{fig:br-cpd2}
	\end{figure}

	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Braunrost CPD/213245"}
		\caption{Fully deflated spore after CPD preparation.}
		\label{fig:br-cpd3}
	\end{figure}

	As these images demonstrate, the fully deflated spores do not seem to be a phenomenon limited of cryo-preparation. 
	
		
	%jeweils das LM-Bild und 3 verschiedene REM-Bilder pro Präparationsmethode auswählen, auf Format achten!, Bildunterschriften
	
	\subsection{OM- and SEM-Images of Mildew}
	
	The growth commonly known as mildew, specifically powdery mildew, is a group of fungi in the order Erysiphales, which are ascomycete fungi. It forms superficial mycelium in order to extract nourishment from its host epidermal cells through absorbing organs called haustoria\cite{alexopoulos1996introductory}. Figure \ref{fig:mehltau-auf-weizen} depicts an OM image of powdery mildew on a wheat leaf. The white colour is characteristic for powdery mildew.
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/LM/Mehltau auf Weizen"}
		\caption{OM image of mildew on a wheat leaf.}
		\label{fig:mehltau-auf-weizen}
	\end{figure}

	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Mehltau CPD/213152"}
		\caption{SEM image of mildew prepared with CPD.}
		\label{fig:md-cpd1}
	\end{figure}

	While figures \ref{fig:md-cpd1} and \ref{fig:md-cryo1} don't show much detail, they provide an overview over the growth for both CPD and cryo-prepared specimen.

	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Mehltau Kryo/212968"}
		\caption{SEM image of mildew based on cryo-preparation.}
		\label{fig:md-cryo1}
	\end{figure}

	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Mehltau CPD/213153"}
		\caption{A closer look at the state of mycelia prepared using CPD.}
		\label{fig:md-cpd2}
	\end{figure}
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Mehltau Kryo/212920"}
		\caption{A closer look at the state of mycelia based on cryo-preparation.}
		\label{fig:md-cryo2}
	\end{figure}

	Comparing figures \ref{fig:md-cpd2} and \ref{fig:md-cryo2} one can clearly see that this time the CPD prepared specimen looks deflated. This is likely the result of complete dehydration.
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Mehltau CPD/213267"}
		\caption{Close up of a haustoria prepared using CPD.}
		\label{fig:md-cpd3}
	\end{figure}

	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Mehltau Kryo/212924"}
		\caption{Close up of a haustoria based on cryo-preparation}
		\label{fig:md-cryo3}
	\end{figure}
	
	The haustoria also visualizes how deflated the CPD samples look when compared to their cryo-prepared counterparts.
	
	%Siehe 4.1
	
	\subsection{OM- and SEM-Images of Spider Mites}	
	
	Spider mites are arachnids of the family Tetranychidae, which are part of the subclass Acari (mites)\cite{spidermite}. They are less than 1mm in size and mostly transparent. They get the spider part of their name from the fact that they spin protective silk webs to protect their colonies from predators (see figure \ref{fig:sm-cpd1})\cite{saito2009plant}. They can damage plants by puncturing the cells to feed.
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{src/LM/gemeine-spinnmilbe-tetranychus-urticae-cc-by-sa-20-by-a-hrefhttpswwwflickrcomph-180508}
		\caption{OM image of two spider mites and an egg}
		\label{spidermite}
	\end{figure}
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Spinnmilbe CPD/213162"}
		\caption{SEM image of spider mite eggs covered in silk prepared with CPD.}
		\label{fig:sm-cpd1}
	\end{figure}

	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Spinnmilbe CPD/213250"}
		\caption{Close up of a spider mite egg prepared using CPD.}
		\label{fig:sm-cpd2}		
	\end{figure}

	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Spinnmilbe Kryo/212966"}
		\caption{Cryo-prepared spider mite egg, showcasing the delicacy of the silk.}
		\label{fig:sm-cryo3}
	\end{figure}

	Comparing figures \ref{fig:sm-cpd2} and \ref{fig:sm-cryo3} showcases that both preparation methods seem to have given about the same results. In both images the egg is still spherical and the delicate silk seems intact.
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Spinnmilbe Kryo/212977"}
		\caption{SEM image of a spider mite based on cryo-preparation.}
		\label{fig:sm-cryo1}
	\end{figure}
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Spinnmilbe Kryo/212962"}
		\caption{Cryo-prepared foot of a spider mite.}
		\label{fig:sm-cryo2}
	\end{figure}
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{"src/Spinnmilbe CPD/213161"}
		\caption{Surface structures of a spider mite prepared using CPD.}
		\label{fig:sm-cpd3}
	\end{figure}

	Figures \ref{fig:sm-cryo1} and \ref{fig:sm-cryo2} give an insight into the anatomy of a spider mite while while figure \ref{fig:sm-cpd3} showcases the interesting surface topography.
	
	%Siehe 4.1
	
\section{Conclusion}

	\subsection{OM - SEM Information}

	When comparing OM and SEM they offer very distinct and different information. While OM  gives colour and transparency information it struggles with the resolution of micro/nanostructures. on the other hand the uniform grey colour of SEM images is a form of information loss, but also helps accentuate structural information and therefore enhances the visibility of the topography. These two technologies complement each other and together help form a more complete picture.
	
\newpage
	
	\subsection{Comparison of Preparation Methods}
	
	As can be seen in chapters 4.1 and 4.2, there is not one superior method. While for the spores of brown rust CPD seemed to be the superior method, it damaged the mycelia of mildew quite extensively compared to cryo-preparation. However one big advantage of cryo-preparation is that its preparation time is by far shorter than that for CPD. This makes it the superior method for many applications where time is of the essence.
	
	\subsection{Problems of SEM}
	
	Due to proper preparation and good technique none of these Images seemed to show any artefacts of charge build-up, or sample damage. Usually this can be a problem, especially when the metallic coating is not perfectly covering or if the acceleration voltage is chosen too high. Additionally when scanning a section for too long or too intensively an insulating layer of carbon starts to build up and darken the image. 
	
	\vskip 1cm
	
	To conclude, we learned a lot from this course about the challenges and techniques of imaging biological samples, the various preparation methods and the inner workings of an SEM. It is a pity that this very practically oriented course could not be done normally, especially since the hands-on experience would have been the most valuable part of it. Nevertheless, we are certain to be richer in experience than we were going into this course.

	%Vergleich LM – REM Information, Vergleich Ergebnisse der beiden Präparationsmethoden, Probleme beim Scannen diskutieren: Aufladungen, Schädigungen
	
\section{Acknowledgements}

	Most importantly I want to thank the Nano Imaging Lab for enabling us gain such valuable experience and also for being able to adapt so quickly given the circumstances. Additionally, I would like to thank the SNI for organizing this blockcourse.
	
\newpage

\bibliographystyle{IEEETRAN}
\bibliography{Bibliography}
	
\end{document}