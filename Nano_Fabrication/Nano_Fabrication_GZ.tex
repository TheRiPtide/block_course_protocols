\documentclass[11pt,a4paper]{article}

\usepackage
[
a4paper,
left=2.5cm,
right=3cm,
top=3cm,
bottom=4cm
]{geometry}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{parskip}
\usepackage[english]{babel}
\usepackage{cite}
\usepackage[numbib,nottoc]{tocbibind}
\usepackage{bbm}
\usepackage{epsfig}
\usepackage{booktabs}
\usepackage{float}
\usepackage{caption}
\usepackage{siunitx}
\usepackage{longtable}
\usepackage{pdfpages}
\usepackage{caption}
\captionsetup[figure]{font=small}
\usepackage{chemformula}
\setchemformula{circled=all}
\setchemformula{circletype=chem}
\usepackage[hidelinks]{hyperref}

\sisetup{
	round-mode          = places, % Rounds numbers
	round-precision     = 3, % to 3 places
}


\setlength{\parindent}{0pt}
\newcommand{\degs}{\ensuremath{^\circ}}

\author{Gregory Zaugg}
\title{Microscopy}

\date{\today}

\begin{document}
	\onehalfspacing	
	\pagenumbering{arabic}
	\setlength\abovedisplayskip{10 pt}
	\setlength\belowdisplayskip{10 pt}
	\righthyphenmin 62
	\lefthyphenmin 62
	
\begin{titlepage}
	\begin{minipage}{.47\textwidth}
		\begin{flushleft}
			\includegraphics[width=3cm]{resources/SNI_black_on_white}
		\end{flushleft}
	\end{minipage}
	\hskip .2\textwidth
	\begin{minipage}{.45\textwidth}
		
		\includegraphics[width=5cm]{resources/UniBas_Logo_EN_Schwarz_RGB_65.pdf}
		

		
	\end{minipage}
	\begin{center}
		\vskip 1.5cm
		\bfseries
		\large Nanoscience block course\\
		\hrulefill \\		
		\vspace{0.5 cm}		
		\textsc{\huge Nano Fabrication} 
		\\ \hrulefill
		\vskip 1.5cm
		\mdseries
		\large 
		{
			\begin{tabular}{rl}
				        \textbf{Author:} & \underline{Gregory Zaugg}, Milan Liepelt, Georg Angehrn \\
				\textbf{Date of course:} & {14.09. - 02.11.2020}                                   \\
				    \textbf{Supervisor:} & Kristopher William Cerveny
			\end{tabular}
		}
		\bfseries
		
	\end{center}
	
	\vskip 1.5cm		
		
\end{titlepage}

\tableofcontents

\newpage

\section{Introduction and Motivation}

Given the importance of electronics in our society and how most modern fronts of research in physics happen at a less than nano scale, it is not difficult to see the importance of fabricating said electronics at that scale. The microelectronics industry uses the term fabrication to describe the creation of complex, highly integrated circuits. It is a process of choosing materials of the desired properties, depositing them, and patterning them in a sequence of steps designed to create an integrated circuit\cite{HARVEY2006303}. In our case it describes a sequence of processes required to create contacts to a sample of Indium Arsenide (InAS) nanowires grown on-top of a Gallium Arsenide (GaAs) wafer. These nanowires have their importance in recent research in the topic of topological q-bits for increased stability in quantum computing. Therefore, a way to reliably contact and then measure the performance of these wires is the first step to making new discoveries. In this report we will document the process of fabricating the aforementioned contacts and make some resistivity measurements on the contacted wires at room temperature and at 4 K. 
	
\section{Fabrication, Experiments and Results}
	
	\subsection{InAs Nanowire Sample}
	
	The sample was produced by the EPFL material science group of Anna Fontcubertai Morral. The InAs nanowires were grown on a GaAs substrate. Each wafer contains 4 samples, each with 4 patches of 6 areas with 4 columns of nanowires (see figure \ref{fig:graphs}a - d). These wires are each 20 $\mu$m in length and spaced 2 $\mu$m apart. The diameter of each nanowire can vary section-wise approximately from 140 nm to 220 nm. 
	
	\begin{figure}[H]
		\centering
		\includegraphics[page=1,trim=0 0 0 0, width=1.2\textwidth]{graphs/Fabrication_graphs_graphs}
		\caption[Overview of one sample:]{Overview of one sample:\\
			
		\hskip .69cm	\begin{tabular}{r l}
			\textbf{a} & Optical Microscope (OM) image of one of the 4 samples per chip. \\
			\textbf{b} & OM image of one patch.                                          \\
			\textbf{c} & OM image of one area with 4 columns of nanowires.               \\
			\textbf{d} & Close-up of several InAs nanowires on the chip.
		\end{tabular}
		}
		\label{fig:graphs}
	\end{figure}
	
	\subsection{Nanowire Contact Fabrication}
	
	To perform measurements on these nanowires, they first need to be contacted. In our case we evaporated gold contacts onto the wires which required some preparation beforehand. From two columns of nanowires, 10 wires each were contacted by 6 contacts at a distance of 600 $\mu $m.
	
		\subsubsection{Mask Fabrication using Electron-Beam Lithography}
	
		In order to have the gold contacts only deposited in the desired areas, the rest of the sample needs to be masked. This is done using PMMA, an electron-beam resist. First the sample was cleaned in acetone and IPA for 5 minutes each and dried on a hot plate at 120 \degs C for 2 minutes. Then the chip was mounted on a spinner and a 4.5\% solution of PMMA was spun on at 4000 rpm for 40 seconds with ramp speed 4. This layer was then baked at 180 \degs C for 7 minutes. A drop of 100 nm gold particles in solution was dabbed on each corner of the chip using a toothpick (see figure \ref{fig:graphs2}a). Those served for orientation and focussing purposes later in the scanning electron microscope (SEM). The rough location of each gold particle cluster was measured using an OM and noted for later use.
		
		The sample was then inserted into a SEM with e-beam lithography capability and the alignment procedure (focussing and angle correction though 3-point alignment) was done using the previously mentioned gold particles. Then the structures were written using a 30 kV acceleration Voltage and an area dose of 200  $\mu$C cm\textsuperscript{-2}. For the 2 mm field (bonding pads) an aperture of 120 $\mu$m was chosen and for the 400 $\mu$m field an aperture of 10 $\mu$m was used. 
		
		For the next step in the mask fabrication the exposed PMMA was developed in a cold developer ((MIBK + IPA):MEK, ratio 100:1:3) at 5 \degs C for 90 seconds, rinsed in IPA for 30 seconds and finally blow-dried with Nitrogen gas. The sample was then inspected under an OM to verify the success of the lithography (see figure \ref{fig:graphs2}b \& c). 
		
		Lastly any residue PMMA that could remain at the contact sites is etched away using reactive ion etching (RIE) (gas: 16\% O2, base pressure: 5e-5 mbar, RF power: 30 W for 1 minute at an operating pressure of 250 mTorr). This process removes around 20 nm of PMMA everywhere, but only really makes a difference where the lithography has etched away most of the resist. At this point the sample is properly masked.
		
		\subsubsection{Native Oxide Etch}
	
		When exposed to oxygen most III-V semiconductors form a thin layer of native oxide over their entire surface. As these oxides are insulators, they obstruct sensible measurements and are therefore undesirable. For that reason, we used the acid \ch{NH4S}\textsubscript{x} to etch away this layer.
		
		First, two beakers of 250 ml deionized (DI) water were prepared, a 20 ml beaker with 10 ml of DI water was set to equilibrate to 40 \degs C on a hot plate and the \ch{NH4S}\textsubscript{x} solution was stirred for 20 minutes.
		
		The electron-beam physical vapour deposition machine (Sharon) was prepared inserting the Ti and Au pockets. 
		
		1 ml of the stirred solution was added to the warm 20 ml beaker of DI water and the sample was immersed in it for 150 seconds. Immediately after that it was transferred to the first 250 ml beaker and swirled for 5-10 seconds. This was repeated for the second beaker. It was then blow dried and stuck to the sample stage plate using two-sided tape and as quickly as possible mounted into the evaporation chamber. For the final preparation step, the vacuum was pumped on said chamber.
		
		\begin{figure}[H]
			\centering
			\includegraphics[page=2,trim=50 40 80 0,clip, width=1\textwidth]{graphs/Fabrication_graphs_graphs}
			\caption[Sample at different stages of fabrication:]{Sample at different stages of fabrication:\\
				
				\hskip .69cm	\begin{tabular}{r l}
					\textbf{a} & OM image of one of the 4 gold nanoparticle clusters.         \\
					\textbf{b} & OM image one sample after e-beam lithography.                \\
					\textbf{c} & Close up of the connection pattern after lithography.        \\
					\textbf{d} & Close-up of the deposited gold connection wires (SEM image).
				\end{tabular}
			}
			\label{fig:graphs2}
		\end{figure}
		
		\subsubsection{Electon-Beam Physical Vapour Deposition (EB-PVD) and Liftoff}
	
		With the sample stage plate mounted and a sufficient vacuum pumped, first 5 nm of Ti were deposited as a sticking layer. Then the main contacts were evaporated on the surface, which consisted of 55 nm Au.
		
		After letting the sample cool down and venting the vacuum it was placed in 50 \degs C warm acetone for at least two hours.
		
		Finally, by creating a bit of turbulence in the acetone using a pipette, all of the resist was removed from the sample and it was left to sonicate at 20\% for 30 seconds to remove any potential residues. After a 1-minute rinse in IPA and a blow dry, the contacts were fully fabricated.
		
	\subsection{Gold Wire Bonding}
	
	To facilitate the insertion of the sample into any measurement device, it was mounted to a chip holder. To establish a connection between the sample and the chip holder a technique called wire bonding is used. 
	
	First the sample was cleaved to fit the chip holder. It was then glued on using a drop of silver paint. The wire bonding machine connects these wires by sonicating a 50 $\mu $m Au wire so it melts to the chip carrier and then drags it over and does the same on the bonding pad. The final result was observed using the OM, see figure \ref{fig:fabricationgraphsfinal}, and the bonding scheme recorded.
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.7\linewidth]{graphs/Fabrication_graphs_final}
		\caption{Gold wires attached to the bonding pads on the sample and to the chip carrier (not visible).}
		\label{fig:fabricationgraphsfinal}
	\end{figure}
		
	\subsection{Measurements at Room-Temperature and 4 K}
	
	To assess the quality of the contacted wires we measured the average resistance of the average of the 20 contacted nanowires (10 from each contacted column, see figures \ref{fig:graphs2}d and \ref{fig:fabricationgraphsfinal}). 
		
		\subsubsection{Measurement Setup}
		
		In order to measure the nanowires, we used the setup seen in figure \ref{fig:experimentsetup}. The digital-analog converter (DAC), both Lock-Ins and both DMMs were connected to the computer for measurement control and data acquisition. A DC signal from the DAC and an AC signal from the Lock-In (LI) 2 were applied to the sample via the SD-Box which combined them beforehand. After the sample the remaining current is converted to a voltage via the IV Converter. This signal is then measured by LI-2 (AC) and DMM-2 (DC). Using the voltage probes V\textsubscript{a} and V\textsubscript{b}, the applied voltage was measured and amplified by the Preamp. This signal was then again read out by LI-1 (AC) and DMM-1 (DC). 
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=\linewidth]{graphs/experiment_setup}
			\caption{Schematics of the measurement setup.}
			\label{fig:experimentsetup}
		\end{figure}
		
		\subsubsection{Measurement Conduction}
		
		At room temperature some basic measurements were done to check which wires can be contacted. Since there are 6 contacts overlaying the nanowires, there should be 15 possible connection variants. But since some contacts didn't seem to work and the signal strength decayed so far over a distance of 1800 nm that only about 5-8 connections per nanowire column were actually measurable. These functioning connections were noted and then measured at 300 K and again at 4 K.
		
		\subsubsection{Measurement Results}
		
		Since due to fabrication errors not all wires were contactable and the conductivity was subpar, a maximum distance of 1800 nm could be measured only once for 300 K and 4 K. Therefore, the error on this measurement was disproportionally large (see figure \ref{fig:nwrt4k}) and it was excluded from the fit. The fit was then interpolated to 0 nm distance to get the system resistance. For 300 K this was a sensible value of 1149.1 $\Omega$, for 4 K this gave a negative value, which physically does not make sense.
		
		Assuming a circular cross-section of the wires with a width of 200 nm, the area though which the current can flow is $A = \pi * r^2 = \pi * (\frac{220nm}{2})^2 = 3.80 * 10^{-14}$
		
		The average conductivity of the 10 wires at room temperature is as follows:
		\begin{equation}
			\sigma_{10, 300K} = \frac{L}{R * A} = \frac{1}{3.69 * 10^{9} \Omega m^{-1}  * 3.80 * 10^{-14}m^2} = 7123.75\; \Omega^{-1}m^{-1}
		\end{equation}
	
		Giving us a single wire conductivity at room temperature of approximately 714 $\Omega^{-1}m^{-1}$
		For the 4 K measurement the same can be done giving a single wire conductivity of approximately 130 $\Omega^{-1}m^{-1}$, though this value should be taken with a grain of salt as according to the fit, the contact resistance is negative.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.85\linewidth]{graphs/NWRT4K}
			\caption{Linear fit of the measurements at 300 K and 4 K}
			\label{fig:nwrt4k}
		\end{figure}		
		
\section{Conclusion and Discussion}

The one thing these results clearly show is that the main goal of this block-course was not to make precise measurements, but to gain insight into common fabrication practices and methods. Nevertheless, I think even in this context it is a stretch to call connecting two data-points a "fit". On top of that if said "fit" results in a value that is nonsensical, it just further solidifies this opinion. We were able to make no more than a handful of measurements between neighbouring contacts, even less in-between contacts spaced one apart and only a single one between contacts spaced two apart. This is likely a result of fabrication and wire bonding errors or it could be that the wires themselves were bad. As visible in the OM images (see figure \ref{fig:graphs}b) the growth, especially for the third row of areas, was not good. Therefore, it makes more sense to discuss the process of fabricating the contacts to the sample. 

This block-course was most valuable in giving students of an otherwise mainly theoretical field the opportunity to gain working experience. Having a mentor that already knows these fabrication methods helped us steer clear of most common mistakes, but still we immediately broke the first chip and destroyed two samples. This course offered a space to make those errors without them actually mattering too much. It taught us how important it is to think things though and plan ahead, as one small mistake can ruin days or even weeks of work, especially the further towards the end of a project it occurs. 

We gained lots of hands on experience with tools and machines we had not previously encountered and learned how to work in a cleanroom, though it would be more accurate to call it a cleaner-than-average-room. Still we were able to experience the pain called wire-bonding first hand, as well as many other joys of fabrication, which makes this block-course a success, at least in my eyes.
	
\section{Acknowledgements}

I would like to thank our supervisor Kristopher William Cerveny, PHD student from the Zumbühllab
at University of Basel for his assistance and guidance in this block-course. It was interesting to gain insight into the work of an experimental physicist, and there was a lot to learn from his experience in the field. I would also like to thank the Swiss Nanoscience Institute for providing the opportunity and organizing this block-course.

\bibliographystyle{IEEETRAN}
\bibliography{Bibliography}
	
\end{document}