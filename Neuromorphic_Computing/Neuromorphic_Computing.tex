\documentclass[11pt,a4paper]{article}

\usepackage
[
a4paper,
left=2.5cm,
right=3cm,
top=3cm,
bottom=4cm
]{geometry}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{parskip}
\usepackage[english]{babel}
\usepackage{cite}
\usepackage[numbib,nottoc]{tocbibind}
\usepackage{bbm}
\usepackage{epsfig}
\usepackage{booktabs}
\usepackage{float}
\usepackage{caption}
\usepackage{siunitx}
\usepackage{longtable}
\usepackage{pdfpages}
\usepackage{caption}
\captionsetup[figure]{font=small}
\usepackage{chemformula}
\setchemformula{circled=all}
\setchemformula{circletype=chem}
\usepackage[hidelinks]{hyperref}

\sisetup{
	round-mode          = places, % Rounds numbers
	round-precision     = 3, % to 3 places
}


\setlength{\parindent}{0pt}
\newcommand{\degs}{\ensuremath{^\circ}}

\author{Gregory Zaugg}
\title{Biocompatible Materials}

\date{\today}

\begin{document}
	\onehalfspacing	
	\pagenumbering{arabic}
	\setlength\abovedisplayskip{2 pt}
	\setlength\belowdisplayskip{10 pt}
	\righthyphenmin 62
	\lefthyphenmin 62
	
\begin{titlepage}
	\begin{minipage}{.45\textwidth}
		\begin{flushleft}
			\includegraphics[width=3cm]{graphs/SNI_black_on_white}
		\end{flushleft}
	\end{minipage}
	\hskip.4\textwidth
	\begin{minipage}{.45\textwidth}
		
		\includegraphics[width=2cm]{graphs/Uni_basel_logo.svg.png}
		
	\end{minipage}
	\begin{center}
		\vskip 1.5cm
		\bfseries
		\large Nanoscience block course\\
		\hrulefill \\		
		\vspace{0.5 cm}		
		\textsc{\huge Nanodevices for Neuromorphic Computing}
		\\ \hrulefill
		\vskip 1.5cm
		\mdseries
		\large 
		{
			\begin{tabular}{rl}
				\textbf{Group:}& \underline{Gregory Zaugg}, Milan Liepelt\\
				\textbf{Date of course:}& {07.09. - 11.09.2020}\\
				\textbf{Supervisors:}& Miklos Csontos, Nadia Jiménez-Olalla
			\end{tabular}
		}
		\bfseries
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=.85\linewidth]{BK_2020/04_memristor_overview}
			\label{fig:04memristoroverview}
		\end{figure}
		
	\end{center}
	
	\vskip 1.5cm
		
		
		
		
\end{titlepage}

\tableofcontents
	
\newpage
	
\section{Abstract}

	In this report we characterize \ch{Ta2O5} based memristors. To further investigate the stability of memristive based systems we parametrize a conventional example memristor to later compare with improved and more sophisticated models. Our results show that there are still lots of improvements to be made in fabrication, but also give insight into what kind of different memristive effects can be classified in this structure.  

\section{Introduction}

	Neuromorphic computing is a method of computer engineering in which elements of a computer are modelled after systems in the human brain. While this can apply for both software and hardware, here we take a closer look at memristor-based neuromorphic hardware. A memristor is a stack of two electrodes with a dielectric in-between. Within this dielectric, conductive filaments can form depending on applied bias voltages, which changes the resistance. This effect called resistive switching results in a pinched hysteresis loop in the IV traces.
	
	With the end of Moore's law approaching and the von Neumann bottleneck still fundamentally limiting conventional architecture, memristor-based neuromorphic hardware aims to solve these problems by eliminating constant data movement, allowing for high degrees of parallelism and creating dynamically changing hardware \cite{zidan_future_2018}. 

	However, one of the main problems that memristor-based systems still face is high device-to-device and cycle-to-cycle variability. This is mostly due to the stochastic nature of conductive filament formation \cite{zidan_future_2018}. The resistive switching technology largely depends on amorphous or polycrystalline dielectric materials that have an uncontrolled high density of defects. These defects are what induce this high degree of variability \cite{adam_challenges_2018}. Another problem that can occur in memristor-based neuromorphic systems are sneak paths. These, however, are not covered in this report.

	The problem that we address here is variability. Through spatial confinement of the switching material in a nano pore we try to restrict the way filaments can form and focus the electric field which is applied over the electrodes. In this report we will, in order to parametrise the superiority of this design, characterise a standard build memristor that uses the same materials as the improved model. 
	
\newpage
	
\section{\ch{Ta2O5} Memristor Fabrication Process}

	The samples used for this report were fabricated in two different ways. For characterisation we mostly used what we will call cross-bar memristors. Later in the electro-forming process we used a different, so-called shadow-mask sample.
	
	\subsection{Cross-bar sample}
	
	The chip was produced on a Si/\ch{SiO2} substrate in three major steps. Each step involved resist (AZ5214E) spinning and consecutive photo-lithography. First, layers of Ti (10nm), Au (100nm) and Ta (40nm) were evaporated using electron-beam physical vapour deposition (EBPVD) to form the bottom electrode (see figure \ref{fig:prod}a). Secondly, an 8nm layer of \ch{Ta2O5} was deposited using high-power impulse magnetron sputtering (HiPIMS) as seen in figure \ref{fig:prod}b. Lastly, layers of Pt (40nm) and Au (160nm) were evaporated using EBPVD to form the top electrode (see figure \ref{fig:prod}c). This resulted in the pattern shown in figure \ref{fig:prod}d.  
	
	\begin{figure}[h!]
		\centering
		\includegraphics[width=1\textwidth]{graphs/prod}
		\caption{ \textit{Schematics of the different layers for fabrication (Top view). \\
				(\textbf{a}) 150nm Bottom electrode (\textbf{b}) 8nm \ch{Ta2O5} layer  (\textbf{c}) 200nm Top electrode  (\textbf{d}) Full pattern on  \ch{Si}/\ch{SiO2} wafer}} 
		\label{fig:prod}
	\end{figure}

	\subsection{Shadow-mask sample}
	
	The fabrication process for this chip was a little less sophisticated and involved no photo-lithography nor resist spinning. Again Si/\ch{SiO2}, wafers served as a substrate. By sputtering Ta in a pure Ar atmosphere a 40nm layer was deposited, covering the entire chip and forming the bottom electrode. Next 10nm of \ch{Ta2O5} were layered on top by sputtering the same Ta target but in the presence of O as well as Ar. After that, scratches were manually made into the oxide layer to access the bottom electrode. Lastly, using a shadow mask created by laser cutting Capton foil the top electrodes were formed from Pt 40nm and Au 300nm with EBPVD.
	
\newpage
	
\section{Characterisation}

	As previously mentioned, only the cross-bar sample was characterised (except for electroforming). 

	\subsection{Optical Microscopy}
	
	To assess the quality of the fabrication we observed and imaged the chip using a Zeiss Imager.M2m optical microscope with a Axiocam
	506 colour microscope camera at varying magnifications.
	
	\subsection{Scanning electron microscope (SEM)}
	
	These pictures were made using a Hitachi S-4800 SEM at an acceleration voltage of 5kV. We used an emission current of 3.5mA and a working distance of 2.1mm.
	
	\subsection{Atomic force microscope (AFM)}
	
	Characterising the height profile and checking the sanity of the deposited materials was done using an AFM (Bruker Icon 3, Tespa V2 k=42 N/m cantilever) in tapping mode.
	
	\subsection{Electroforming (IV measurements)}
	
	The setup as displayed in figure \ref{fig:measurementsetup} consisted of a digital-analogue converter (DAC) providing an output voltage, a series resistor, the chip itself in a sample holder and an IV converter. Using a LabView program as control software we were able to provide an output of $\pm10$V from the DAC. We applied a sawtooth shaped signal over the system. In our case a positive bias means higher potential on the tantalum electrode. The $V_{out}$ was fed straight back into the DAC to account for deviations in the output. The series resistor was mostly there to protect the IV converter in case of a breakthrough of the memristor.
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=1\linewidth]{graphs/measurement_setup.pdf}
		\caption{Schematics of the electroforming setup}
		\label{fig:measurementsetup}
	\end{figure}

	The series resistor ranged from 4k$\Omega$ to 20k$\Omega$ depending on the desired voltage drop over the sample. 
	
	\newpage
	
	To start with, the 20k$\Omega$ resistor was used and the sweep frequency was set to 1Hz. For the shadow-mask sample we usually began with $\pm$1V and continued increasing with increments of 0.1V until the first soft breakdown was reached. This was generally around 2.9V. Further the resistor was exchanged for the 10k$\Omega$ resistor and the voltage was again step by step raised up to 9V. For some frequency variance we also tried ramping up to 400Hz. 
	
	For the cross-bar sample the frequency was also set to 1Hz and we started at 0.1V with the 20k$\Omega$ resistor. Soft breakdown occurred at around 1V. Back-sweeping (only applying negative voltages) up to -2.2V was able to restore the hysteresis, though soft breakdowns then occurred at lower voltage. Hard breakdown (non-recoverable) was usually around 2V.
	

\section{Results and Conclusion}	

	\subsection{Fabrication quality and quantity}
	
	Due to some lift-off issues during the fabrication of the cross-bar samples, we had to count the number of connected memristors per chip using optical microscopy. This means both the bottom and top electrode needed to overlap and be connected to the large pads near the edge of the chip. A perfect chip would have contained 32 connected memristors. Only two chips seemed to have any functional looking memristor, we will call them chip 1 with nine and chip 2 with 4 total memristors.
	
	\subsection{\ch{Ta2O5} layer}
	
	A further part of the characterisation involved assessing the \ch{Ta2O5} (from now on referred to as oxide) layer. Optically we were able to confirm its existence as can be seen in figure \ref{fig:fulloverview}.2a from the different colour (purple) compared to the \ch{SiO2} (blueish). But when checking with the AFM we were unable to confirm any layer at all, neither on top of the electrode (\ref{fig:fulloverview}.4a - c) nor on the wafer itself (\ref{fig:fulloverview}.3a - c). On the contrary, \ref{fig:fulloverview}.3b actually shows a dip in height where a 8nm step should be. \\
	We believed this to be due to the different resonance properties that these materials could have when analysed in tapping mode. Due to the lack of contact-mode cantilever tips we were unable to fully explain this issue.
	
	The second measurement on the electrode itself also showed only minimal difference between where we expected to see the oxide versus the pure electrode itself. This is best visualized in 4c as one can observe a lack of big spikes in the last segment on the x axis. This is further supported in the roughness analysis of figure \ref{fig:fulloverview}.4b and table \ref{tab:roughness}. \ref{fig:fulloverview}.4b shows the roughness profile of the oxide in red and the one of Gold in black.
	
	
	\vskip 0.5cm
	\begin{table}[h!]
		\begin{center}
			\begin{tabular}{|l|l|r|r|}
				\hline
				\textbf{Location} & \textbf{Surface material} & \textbf{ $R_a$ [pM]} & \textbf{ $R_q$ [pM]} \\ \hline
				Bottom electrode  & Au                        &               114 &               143 \\
				Oxide-layer       & \ch{Ta2O5}                &               146 &               191 \\ \hline
			\end{tabular}
		\caption{Average roughness (Ra) and Root mean square roughness (Rq) comparison of the border between the bottom electrode and the oxide-layer on top. }
		\label{tab:roughness}
		\end{center} 
	\end{table}	
	
	
	
	\begin{figure}[H]
		\centering
		\includegraphics[trim=130 0 0 0, width=1.2\textwidth]{graphs/graphs}
		\caption{Complete overview of a memristor. \\
				 (\textbf{1}) Optical microscope image (5x magnification)\\
			 	 (\textbf{2a \& b}) Optical microscope and SEM image of one memristor ($\sim$50x magnification, black bar is 50 $\mu$m)\\
			 	 (\textbf{3a - c}) AFM-measurement at the border between \ch{Ta2O5} and \ch{SiO2} (Profile scale is in $\mu$m)\\
			 	 (\textbf{4a - c}) AFM-measurement and roughness analysis at the edge of the \ch{Ta2O5} layer on top of the bottom electrode \\
			 	 (\textbf{5a - c}) Rotated AFM-measurement over the memristor (Profile scale is in $\mu$m)
		 	 	}
		\label{fig:fulloverview}
	\end{figure}

	
	\subsection{Electrode dimensions}
	
	A simple check on the dimensions of both electrodes allowed us to confirm that the EBPVD went without visible complications. Figure \ref{fig:fulloverview}.5b shows the height profile of the overlap between bottom and top electrode. 
	
	The heights measured seem to line up with what was intended and there is a good overlap between the part of the top electrode that is on the bottom electrode and the part that is on the substrate (see 5c). 
	
	\subsection{IV-Characteristics and memristive behaviour}
	
	The results produced from the two different types of samples differ by a fair bit, which is why they will be discussed separately. 
	
	\subsubsection{Shadow-mask sample}
	As can be seen in figure \ref{fig:frequencyvariance}, the behaviour is clearly non-linear with a hysteresis loop opening up on both sides. The effect, however, was much larger in the negative range. By increasing the frequency this loop could be opened up. This seemed to be a very stable effect as 100 repetitions did not deviate much. 
	
	\vskip 1.7cm
	
	\begin{figure}[H]
		\centering
		\includegraphics[page=2, trim=80 70 80 80, width=1.22\textwidth]{graphs/graphs}
		\caption{Schottky non-linear memristive behaviour with increased frequencies opening up the hysteresis loop. Hysteresis loop at (\textbf{a}) 4Hz, (\textbf{b}) 60Hz, (\textbf{c}) 100Hz and (\textbf{d}) 400Hz}
		\label{fig:frequencyvariance}
	\end{figure}

	This shape however does not seem to resemble uni- or bipolar switching. It is more comparable to hysteresis loops found in nanowires as reported by Jie Guo et al. and Yong Zhou et al. who associate this with Schottky effects \cite{guo_reconfigurable_2013}\cite{zhou_modulating_2016}.
	
	\subsubsection{Cross-bar sample}
	The cross-bar sample however showed very different behaviour. Here there are very strong signs of bipolar resistive switching with a negative set voltage \cite{choi_unified_2017}. The effect is not very stable but we were able to produce some cycles for analysis (see figure \ref{fig:bipolarswitching}).  Shown here is a sweep from +1.2V to -1.2V at 1Hz with a series resistor of 20k$\Omega$. At -1.1V the memristor was set into the On state. The resistance R at this point is found through fitting on the data from figure \ref{fig:bipolarswitching}e and calculated to be as given in equation \ref{eq:on}. The conductivity G is obtained through the fitted slope. For details concerning the fit see figures \ref{fig:onfit} and \ref{fig:offfit} in the Appendix.
	
	
	\vskip 3cm
	
	\begin{figure}[H]
		\centering
		\includegraphics[page=3, trim=80 10 0 100, width=1.81\textwidth]{graphs/graphs}
		\caption{Bipolar resistive switching with a negative set voltage}
		\label{fig:bipolarswitching}
	\end{figure}

	\begin{equation}
		R = \frac{1}{G} = \frac{1}{1.76855*10^{-4} \pm 1.63051*10^{-7}} = 5654 \pm 5 \Omega
		\label{eq:on}
	\end{equation}

	Through figure \ref{fig:bipolarswitching}e we can see that the conductivity in the Off state is not equal to zero, therefore we can fit and calculate it via equation \ref{eq:off}.
	
	\begin{equation}
		R = \frac{1}{G} = \frac{1}{4.78293*10^{-7} \pm 4.11953*10^{-9}} = 2.091 \pm 0.018 M\Omega
		\label{eq:off}
	\end{equation}

	Due to the unstable manner of these two states we were unable to produce any results on retention time. This itself is already an indicator that it might not be a long period of time. We were able to reproduce this in some other measurements but not with any degree of predictability.
	
	\subsection{Conclusion}
	In conclusion, we were able to produce two completely different behaviours in the two samples, none of which we were expecting and hoping for. We are not sure how either of those came to be, but we suspect that at least for the cross-bar sample the thin oxide layer could cause unexpected behaviour. This, however, we cannot say for certain as the characterisation of the oxide layer proved to be more difficult than expected. To more reliably measure the thickness of the oxide layer we suggest using an AFM in contact mode and additionally checking the quality using energy dispersive x-ray SEM (SEM-EDX).
	
	As an outlook we would hope to see a second production of samples that uses the knowledge gained from this report to improve upon the fabrication process and offer the results that are desired.
	 
\section{Acknowledgements}

	I would like to thank our supervisors Miklos Csontos and Nadia Jiménez-Olalla for their guidance and assistance during this project. It was a great experience being able to work on such a thrilling and important project with such amazing people. Lastly I would like to thank Milan Liepelt for his help with creating nice visualizations and his amazing research to analyse our findings.

\newpage

\bibliographystyle{IEEETRAN}
\bibliography{Bibliography}
	
\section{Appendix}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{graphs/off_fit}
	\caption{Linear fit on the Off state of a cross-bar memristor sample. Forced intercept at 0. Blue represents fitted line, red points are masked.}
	\label{fig:offfit}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{graphs/on_fit}
	\caption{Linear fit on the On state of a cross-bar memristor sample. Forced intercept at 0. Blue represents fitted line, red points are masked. Not continuous after x intercept due to temporal separation of data points.}
	\label{fig:onfit}
\end{figure}



	
\end{document}