\documentclass[11pt,a4paper]{article}


\usepackage
	[
	a4paper,
	left=2.5cm,
	right=3cm,
	top=3cm,
	bottom=4cm
	]{geometry}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{parskip}
\usepackage[english]{babel}
\usepackage{cite}
\usepackage[numbib,nottoc]{tocbibind}
\usepackage{bbm}
\usepackage{epsfig}
\usepackage{booktabs}
\usepackage{float}
\usepackage{caption}
\usepackage{siunitx}
\usepackage{longtable}
\usepackage{pdfpages}
\usepackage{caption}
\captionsetup[figure]{font=small}
\usepackage{chemformula}
\setchemformula{circled=all}
\setchemformula{circletype=chem}
\usepackage[hidelinks]{hyperref}

\sisetup{
	round-mode          = places, % Rounds numbers
	round-precision     = 3, % to 3 places
}


\setlength{\parindent}{0pt}
\newcommand{\degs}{\ensuremath{^\circ}}

\author{Gregory Zaugg}
\title{Biocompatible Materials}

\date{\today}

\begin{document}
\onehalfspacing	
\pagenumbering{arabic}
\setlength\abovedisplayskip{2 pt}
\setlength\belowdisplayskip{10 pt}
\righthyphenmin 62
\lefthyphenmin 62

\begin{titlepage}
	\begin{minipage}{.45\textwidth}
		\begin{flushleft}
			\includegraphics[width=3cm]{graphs/SNI_black_on_white}
		\end{flushleft}
	\end{minipage}
	\hskip.4\textwidth
	\begin{minipage}{.45\textwidth}

		\includegraphics[width=2cm]{graphs/Uni_basel_logo.svg.png}
		
	\end{minipage}
	\begin{center}
		\vskip 1.5cm
		\bfseries
		\large Nanoscience block course
		\vskip 1cm
		\hrulefill \\
		\vspace{0.5 cm}
		\textsc{\huge Biocompatible Materials}
		\\ \hrulefill
		\vskip 1.5cm
		\mdseries
		\large 
		{
			\begin{tabular}{rl}
				\textbf{Group:}& \underline{Gregory Zaugg}, Tamara Utzinger\\
				\textbf{Date of experiment:}& {31.08 - 04.09.2020}\\
				\textbf{Supervisors:}& Joachim Köser, Theodor Bühler, Federico Dalcanale
			\end{tabular}
		}
		\bfseries
		
		\vskip 4cm

	\end{center}

	
	\vskip 1.5cm
	
	
	
	
\end{titlepage}

	\tableofcontents
	
\newpage

\section{Introduction}

	Biocompatibility as a concept and material science has evolved rapidly over the last decades. At this point it is important to distinguish between the general term of biocompatibility and the term in context of biomedical therapy. Generally it is defined as the "Ability to be in contact with a living system without producing an adverse effect." \cite{IUPAC2012}. However, for biomedical applications this is not sufficient. There the material should have the ability to perform with an appropriate host response in a specific application \cite{IUPAC2012}. As for this block course, this is exactly what we will be attempting and studying.
	
	The purpose of this report is not to reinvent the wheel but rather to offer insight into some important and widely used procedures. We will focus on the process of fabricating and characterizing four different surface modifications on titanium samples, a material already well-known for its high biocompatibility. 
	The three different aspects that we will touch upon are the methods of fabricating said samples, analysing their effects on eukaryotic cells and characterizing their properties using several physico-chemical procedures and measurements.
		
\section{Methods of Surface Modification}

	To begin, four titanium samples were prepared in different ways. This gave insight into various methods of surface functionalization that are representative for a group of similar processes. All of these methods are already well known for their biocompatibility. Different fields of biomedical engineering prefer certain methods, as for example calcium plated samples can bond exceptionally well to bone but should not be used for temporary implants that need to be removed later. Surface roughness also affects cell morphology which has to be taken into account and will be studied later in this report.

	\subsection{Anodizing}
	
		This is an electrolytic process. It increases the thickness of the natural oxide layer in order to make it more corrosion resistant, harder to scratch and/or add a cosmetic visual effect. In our case this was done using a 1 molar solution of \ch{H3PO4}. The samples were set anodic and dipped into said solution for approximately 1 minute while applying a distinct voltage to each sample. Different voltages lead to different thicknesses of oxide layers, which in turn affects the refraction of incoming light and therefore the surface colour, as visualized in figure \ref{fig:titaniumanodization}. We used voltages of 60V, 120V and 150V.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.7\linewidth]{graphs/titanium_anodization}
			\caption{Titanium interference patterns on the anodized surface.\cite{intro}}
			\label{fig:titaniumanodization}
		\end{figure}
		
	
	\subsection{Calcium Depositing}
	
		Another electrolytic process called electroplating was used in this method. The samples were placed in a calcium-hydroxide solution (0.02 M \ch{Ca(OH)2}) in an ultrasound bath.To increase solubility the pH was lowered using phosphoric acid (0.012 M \ch{H3PO4}; pH 4.8). With the sample set as the cathode a voltage of 20V was applied. Over the course of 10 minutes a layer of calcium was deposited on the sample surface. The ultrasound bath helped remove any bubbles of gas forming at the reaction site, which would block more calcium from reaching it. 
	
	\subsection{Trovalising}
	
		This is a mechanical process involving the rotary tumbling of the sample in a pot filled with pellets of media over a certain period of time. In our case prism-shaped abrasive ceramic stones of around 5mm length were used and the sample was left in the rotating medium for approximately 1 hour. This provides a matte but even finish. The samples were cleaned in an ethanol-ultrasound-bath to remove residue abrasive. 
	
	\subsection{Sandblasting}
	
		Similar to trovalising this is another process involving mechanical abrasion of the sample surface. In this case pressurized air is blasting a fine powder onto the sample. Depending on the size, shape and hardness of the abrasive particles, different finishes can be produced. In our case a sand-like substance (likely \ch{Al2O3}) was used for a rough finish. The same cleaning process as with trovalising is was applied. 
	
\section{Biocompatibility Evaluation}

	There exist several standard procedures to evaluate the biocompatibility of our prepared samples. Especially for any implant it is very important to analyse the cytotoxicity as to not produce any unwanted effects. 

	\subsection{Cytotox Assay}
	
		\subsubsection{Preparation}
		
		To assess the biocompatibility of a sample it is common practice to do a Cytotoxicity Assay. It helps to parametrize the impact of a medium on cell growth.
		This cytotox assay (CA) was conducted mostly after the ISO 10933-5 standard\cite{ISO10933}. We used a mammalian cell culture of L929 mouse fibroblasts. After checking the condition of the cells visually under the microscope (See figure \ref{fig:l929} and \ref{fig:rat2}), they were detached using a solution of trypsin. The sample was sterilized using 70\% EtOH. With a Neubauer counting chamber the cell concentration was calculated and approximately 100'000 cells were seeded onto each sample in a nutrient solution containing vitamins, antibiotics, 10\% serum and other nutrients. Additionally two control groups were added, one with a piece of a latex glove, which is known for its cytotoxicity, and the other without any media. The latter served as a reference value. These cells were then incubated at 37\degs C for approximately 24 hours.
		
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.6\linewidth]{data/L929_2_tamara_gregory}
			\caption{L929 mouse fibroblast cells used for this  CA}
			\label{fig:l929}
		\end{figure}
		
		In order to analyse the cell morphology a second culture of Rat2 rat fibroblast cells were prepared and incubated in the same way.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.6\linewidth]{data/rat2_tamara_gregory}
			\caption{Rat2 rat fibroblast cells used for the morphology analysis}
			\label{fig:rat2}
		\end{figure}
		
		\subsubsection{Resazurin Assay}
		
		First the condition of the two reference groups was checked. As expected the majority of cells in the latex sample solution were dead and the cells that were left to grow without any external media appeared healthy. Then to prepare the cells they were stained with the phenoazine dye Resazurin. This was used to parametrize the metabolic rate of our cell cultures. Fluorescence measurements were taken after two and six hours.  A resazurin solution served as the base 0\% value and the aforementioned reference value gave us a 100 \% parameter.This blue dye is weakly fluorescent, non-toxic, cell-permeable, and redox‐sensitive \cite{resazurin_tracer} \cite{resazurin_photochem}. Resazurin is reduced by aerobic respiration of metabolically active cells to highly fluorescent pink-coloured resorufin. Resorufin has an excitation maximum at 530-570 nm and an emission maximum at 580-590 nm \cite{resorufin_fluorescence}. 
		
		\begin{minipage}{0.5\textwidth} 
		 	\begin{figure}[H]
		 		\centering
		 		\includegraphics[width=0.9\linewidth]{graphs/vitality_2h}
		 		\caption{Fluorescence intensity after two hours}
		 		\label{fig:vit2}
		 	\end{figure}
		\end{minipage}
	 	\begin{minipage}{0.5\textwidth} 
	 		\begin{figure}[H]
	 			\centering
	 			\includegraphics[width=0.9\linewidth]{graphs/vitality_6h}
	 			\caption{Fluorescence intensity after six hours}
	 			\label{fig:vit6}
	 		\end{figure}
	 	\end{minipage}
 	
 		\vskip 1cm
		
		 As seen in figures \ref{fig:vit2} and \ref{fig:vit6} neither anodizing nor calcium plating seem to have a large adverse effect and trovalising as well as sand-blasting seem to have boosted the growth rate by a few percent, though this could also be due to slight inaccuracies during preparation. What is clearly visible however is the cytotoxicity of latex. 
		
		\subsubsection{Morphology Analysis}
		
		To see how the Rat2 cells interact with the different surfaces of each sample the actin-filaments and the nuclei were each tagged with a fluorescent dye. To do so the cells were prepared first. A formalin solution was used to fixate them and then they were made permeable with a Triton X-100 solution. Then the two dyes phalloidin and 4,6-diamidino-2-phenylindole (DAPI) were added. Phalloidin tags to the actin-filaments of the cells and DAPI tags DNA. To stabilize the cells they were embedded in a mowiol, a synthetic polymer. These stained cells were then analysed under a Fluorescence Confocal Laser Scanning Microscope (see figures \ref{fig:fluo_ca}-\ref{fig:fluo_sb}).

	\subsection{Physico-chemical Analyisis}
	
		To analyse the chemical composition of the deposited calcium crystals a series of measurements were taken. Using SEM-EDX we analysed the mass composition of the surface layer to compare mostly the ratio between calcium and phosphor. Fourier-Transform InfraRed (FTIR) spectroscopy then gave us closer insight into what molecule we possibly deposited.
		
		\subsubsection{SEM-EDX}
		
		Scanning Electron Microscope Energy-Dispersive X-Ray (SEM-EDX) spectroscopy utilizes characteristic X-Rays that are produced by the incident electron-beam of the SEM. This has to be taken into account when analysing EDX data, since not only the surface but also some material below it are measured.
	
		This produces a spectrum per spot which shows the exact atomic composition per weight, which can then be converted to molarity via the atomic mass of each element, see figures \ref{fig:edxoverview} and \ref{fig:edxspec}. 
	
		\begin{minipage}{0.4\textwidth} 
			\begin{figure}[H]
				\centering
				\includegraphics[width=0.9\linewidth]{data/EDX_overview}
				\caption{Overview of the spots where the two measurements were taken}
				\label{fig:edxoverview}
			\end{figure}
		\end{minipage}
		\begin{minipage}{0.6\textwidth} 
			\begin{figure}[H]
				\centering
				\includegraphics[width=0.9\linewidth]{data/EDX_spec}
				\caption{Overlay of the two individual spectra}
				\label{fig:edxspec}
			\end{figure}
		\end{minipage}
	
		
		\begin{table}[H]
			\centering
			\begin{tabular}{|l|l|l|l|l|}
				\hline
				Element & Pt.1 Atom \% & Pt.2 Atom \% $\sigma$ & Pt.2 Atom \% & Pt.2 Atom \% $\sigma$ \\ \hline\hline
				C       & 3.13         & $\pm$0.10             & 2.57         & $\pm$0.09             \\ \hline
				O       & 53.55        & $\pm$0.63             & 65.87        & $\pm$0.57             \\ \hline
				P       & 6.86         & $\pm$0.07             & 8.69         & $\pm$0.08             \\ \hline
				Ca      & 9.84         & $\pm$0.06             & 8.92         & $\pm$0.05             \\ \hline
				Ti      & 26.23        & $\pm$0.15             & 13.95        & $\pm$0.10             \\ \hline
			\end{tabular}
			\caption{table of the most important Elements detected.}
			\label{tab:edx}
		\end{table}
		
		Using the data given in table \ref{tab:edx} we can calculate the ratio Ca/P to be $1.206 \pm 0.010$.
		Further it can be used to image the percentage distribution of each element and create a map of all elements:
		
		\begin{minipage}{0.33\textwidth} 
			\begin{figure}[H]
				\centering
				\includegraphics[width=0.9\linewidth]{data/edxpic}
				\caption{SEM overview}
				\label{fig:edxpic}
			\end{figure}
		\end{minipage}
		\begin{minipage}{0.33\textwidth} 
			\begin{figure}[H]
				\centering
				\includegraphics[width=0.9\linewidth]{data/edxca}
				\caption{Ca distribution}
				\label{fig:edxca}
			\end{figure}
		\end{minipage}
		\begin{minipage}{0.33\textwidth} 
			\begin{figure}[H]
				\centering
				\includegraphics[width=0.9\linewidth]{data/edxp}
				\caption{P distribution}
				\label{fig:edxp}
			\end{figure}
		\end{minipage}
	
		\vskip 1cm
	
		Images \ref{fig:edxpic}, \ref{fig:edxca} and \ref{fig:edxp} also show that the spatial distribution of Ca and P matches up with each other and also with the location of the crystals in the image.
		
		\subsubsection{FTIR}
		
		To further analyse our so far unknown substance we scraped off a few flakes and analysed them with Fourier Transform InfraRed spectroscopy (FTIR). This gave us the absorption spectrum in figure \ref{fig:ftirspec}.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.9\linewidth]{data/ftir_spec}
			\caption{Infrared absorption spectrum of our Ca/P sample}
			\label{fig:ftirspec}
		\end{figure}
	
		\subsection{Topology and Roughness Analysis}
		
		Roughness is a very important factor for biocompatible materials, especially if biological tissue is supposed to bind to it. Important here is the scale of the topological features, as it defines what kind of interactions can occur\cite{intro}. 
		
		\subsubsection{Confocal Laser Scanning Microscope}
		
		Using a Confocal Laser Scanning Microscope (CLSM) we analysed the roughness of each sample. First the calcium sample in figure \ref{fig:ca_clsm}. We specifically chose a spot that had underlying titanium showing as to see the difference:
		
		\begin{figure}[H]
			\centering			
			\begin{tabular}{c c c}
				\includegraphics[width=45mm]{data/Ca_gold}&
				\includegraphics[width=45mm]{data/Ca_heat}&
				\includegraphics[width=45mm]{data/Ca_img}
			\end{tabular}
			\caption{CLSM images of the Ca sample}	
			\label{fig:ca_clsm}		
		\end{figure}
	
		Next was the anodized sample, you can clearly see the spectral pattern of the surface in the top down image of figure \ref{fig:a_clsm}
	
		\begin{figure}[H]
			\centering
			\begin{tabular}{c c c}
				\includegraphics[width=45mm]{data/A_gold}&
				\includegraphics[width=45mm]{data/A_heat}&
				\includegraphics[width=45mm]{data/A_img}
			\end{tabular}
%			\caption{CLSM images of the A sample}
			\label{fig:a_clsm}			
		\end{figure}
		
		Further, the trovalized sample in figure \ref{fig:t_clsm}:
		
		\begin{figure}[H]
			\centering
			\begin{tabular}{c c c}
				\includegraphics[width=45mm]{data/T_gold}&
				\includegraphics[width=45mm]{data/T_heat}&
				\includegraphics[width=45mm]{data/T_img}
			\end{tabular}
			\caption{CLSM images of the T sample}
			\label{fig:t_clsm}			
		\end{figure}
		
		And lastly the sand-blasted sample as seen in figure \ref{fig:sb_clsm}:
		
		\begin{figure}[H]
			\centering
			\begin{tabular}{c c c}
				\includegraphics[width=45mm]{data/SB_gold}&
				\includegraphics[width=45mm]{data/SB_heat}&
				\includegraphics[width=45mm]{data/SB_img}
			\end{tabular}
			\caption{CLSM images of the SB sample}
			\label{fig:sb_clsm}			
		\end{figure}
	
		\begin{table}[H]
			\centering
			\begin{tabular}{| l | r | r | r | r |}
				\hline
				Sample           & Ca         & A          & T          & SB         \\ \hline
				$\overline{R_a}$ & 4.118      & 0.418      & 0.375      & 1.961      \\
				$\sigma_{R_a}$   & $\pm$1.185 & $\pm$0.091 & $\pm$0.043 & $\pm$0.254 \\ \hline
			\end{tabular}
			\caption{Average roughness value of each sample (averaged over 4-5 lines)}
			\label{tab:Ra}			
		\end{table}
	
		The values of table \ref{tab:Ra} paired with the images from figure \ref{fig:ca_clsm} and \ref{fig:sem_ca}  show that the inhomogeneous and crystalline nature of the calcium sample results in a large roughness. This very likely impacted its ability to interact with the L929 cells and partially caused the reduced vitality as seen in figure \ref{fig:vit2} and \ref{fig:vit6}. 
	
		\subsubsection{SEM}
		
		To take some proper high resolution images of each surface we looked at them with the electron microscope.
		
		\begin{minipage}{0.5\textwidth}
			\begin{figure}[H]
				\centering
				\includegraphics[width=0.8\linewidth]{"data/S4-gr1_ca_05"}
				\caption{Calcium sample}
				\label{fig:sem_ca}
			\end{figure}
		\end{minipage}
		\begin{minipage}{0.5\textwidth}
			\begin{figure}[H]
				\centering
				\includegraphics[width=0.8\linewidth]{"data/S1-anod-gr1_07"}
				\caption{Anodized sample}
				\label{fig:sem_a}
			\end{figure}
		\end{minipage}
		\begin{minipage}{0.5\textwidth}
			\begin{figure}[H]
				\centering
				\includegraphics[width=0.8\linewidth]{"data/S1-trov-gr1_02"}
				\caption{Trovalised sample}
				\label{fig:sem_t}
			\end{figure}
		\end{minipage}
		\begin{minipage}{0.5\textwidth}
			\begin{figure}[H]
				\centering
				\includegraphics[width=0.8\linewidth]{"data/S1-sb-gr1_13"}
				\caption{Sand-Blasted sample}
				\label{fig:sem_sb}
			\end{figure}
		\end{minipage}
	
		Figures \ref{fig:sem_ca}-\ref{fig:sem_sb} nicely visualise the nature of the different surface treatments. The crystalline structure of the calcium sample, the smooth and almost unaltered surface of the anodized sample, the smoothed and lightly dented trovalised sample as well as the very rough and jagged finish on the sand-blasted sample.
	
\section{Discussion \& Conclusion}	

	First off, none of these results were really unexpected or new. They have their purpose in showcasing real world applications. True to the maxim "The journey is the destination" we learned a lot more during the making of our results than from the results themselves. That said there are still some results to discuss. For example while the roughness of the anodized, trovalised and sand-blasted samples are quite different, they perform roughly the same regarding cytotoxicity, as seen in table \ref{tab:Ra} and figure \ref{fig:vit6}. Additionally, the calcium sample performed poorly and had a non negligible impact on the cell vitality. Surprisingly however this could not be seen in the fluorescence CLSM as cell morphology looked about the same as for the other samples (see figures \ref{fig:fluo_ca}-\ref{fig:fluo_sb}). 
		
	When comparing the spectrum in figure \ref{fig:ftirspec} to reference values, such as in figure \ref{fig:ftirref} (see appendix) we see that the spectrum of DCPD seems to match rather well. This is also more or less supported by the EDX data, as DCPD (\ch{CaHPO4}.2\ch{H2O}) has a 1.00 ratio of Ca/P (see figure \ref{fig:capref}) and our ratio of $1.206 \pm 0.010$. Though this might be closer to OCP, the spectra don't match up at all \cite{ben2014advances}.
	
	This shift is rather interesting as the expected value of the ratio is not within the known margin of error. We suspect this could be due to other high calcium composites that may have formed but are not frequent enough to have a visible impact on the IR absorption. Also a sample size of n = 2 measurements is not representative.
	
	To conclude, the cytotoxicity assay gave us a first insight into a biolab, the FTIR and EDX let us experience physico-chemical analysis firsthand, through CLSM we learned about roughness evaluation and last but not least using the SEM gave us some presentable images, all very interesting processes that offered us practical experience to use in our further careers.
	 
\section{Acknowledgements}

	I thank professor Uwe Pieles and professor Michael de Wild for making their
	laboratory available for this block course. Additionaly, I would like to thank the SNI
	for giving us the opportunity to make such valuable experiences. But most of all I thank Joachim Koeser, Theodor
	Bühler and Federico Dalcanale for taking the time to teach us all the different
	methods.
	
\bibliographystyle{IEEETRAN}
\bibliography{Bibliography}

\section{Appendix}

	\begin{figure}[H]
		\centering
		\includegraphics[width=0.7\linewidth]{data/ftir_ref}
		\caption{Different IR spectra of Ca/P coatings\cite{pierre2019calcium}}
		\label{fig:ftirref}
	\end{figure}

	\begin{figure}[H]
		\centering
		\includegraphics[width=0.7\linewidth]{data/CaP_ref}
		\caption{Different Ca/P ratios of synthetic and biological calcium phosphates\cite{ben2014advances}}
		\label{fig:capref}
	\end{figure}

	\begin{minipage}{0.45\textwidth}
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.8\linewidth]{"data/Ca_20x_actin_and_nuclei_C002"}
			\caption{Fluorescence image of the
				cells on the calcium sample}
			\label{fig:fluo_ca}
		\end{figure}
	\end{minipage}\hskip 0.1\textwidth
	\begin{minipage}{0.45\textwidth}
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.8\linewidth]{"data/A_20x_actin_and_nuclei_C002"}
			\caption{Fluorescence image of the
				cells on the anodized sample}
			\label{fig:fluo_a}
		\end{figure}
	\end{minipage}

	\begin{minipage}{0.45\textwidth}
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.8\linewidth]{"data/T_20x_actin_and_nuclei_C002"}
			\caption{Fluorescence image of the
				cells on the trovalised sample}
			\label{fig:fluo_t}
		\end{figure}
	\end{minipage}\hskip 0.1\textwidth
	\begin{minipage}{0.45\textwidth}
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.8\linewidth]{"data/SB_20x_actin_and_nuclei_C002"}
			\caption{Fluorescence image of the
				cells on the sand-blasted sample}
			\label{fig:fluo_sb}
		\end{figure}
	\end{minipage}
	
\end{document}