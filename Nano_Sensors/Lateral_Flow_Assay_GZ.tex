\documentclass[11pt,a4paper]{article}

\usepackage
[
a4paper,
left=2.5cm,
right=3cm,
top=3cm,
bottom=4cm
]{geometry}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{parskip}
\usepackage[english]{babel}
\usepackage{cite}
\usepackage[numbib,nottoc]{tocbibind}
\usepackage{bbm}
\usepackage{epsfig}
\usepackage{booktabs}
\usepackage{float}
\usepackage{caption}
\usepackage{siunitx}
\usepackage{longtable}
\usepackage{pdfpages}
\usepackage{caption}
\captionsetup[figure]{font=small}
\usepackage{chemformula}
\setchemformula{circled=all}
\setchemformula{circletype=chem}
\usepackage[hidelinks]{hyperref}

\sisetup{
	round-mode          = places, % Rounds numbers
	round-precision     = 3, % to 3 places
}


\setlength{\parindent}{0pt}
\newcommand{\degs}{\ensuremath{^\circ}}

\author{Gregory Zaugg}
\title{Microscopy}

\date{\today}

\begin{document}
	\onehalfspacing	
	\pagenumbering{arabic}
	\setlength\abovedisplayskip{10 pt}
	\setlength\belowdisplayskip{10 pt}
	\righthyphenmin 62
	\lefthyphenmin 62
	
\begin{titlepage}
	\begin{minipage}{.47\textwidth}
		\begin{flushleft}
			\includegraphics[width=3cm]{resources/SNI_black_on_white}
		\end{flushleft}
	\end{minipage}
	\hskip .2\textwidth
	\begin{minipage}{.45\textwidth}
		
		\includegraphics[width=5cm]{resources/UniBas_Logo_EN_Schwarz_RGB_65.pdf}
		

		
	\end{minipage}
	\begin{center}
		\vskip 1.5cm
		\bfseries
		\large Nanoscience block course\\
		\hrulefill \\		
		\vspace{0.5 cm}		
		\textsc{\huge Lateral Flow Assay} 
		\\ \hrulefill
		\vskip 1.5cm
		\mdseries
		\large 
		{
			\begin{tabular}{rl}
				       \textbf{Authors:} & \underline{Gregory Zaugg}, Timon Flathmann, Philipp Van der Stappen \\
				\textbf{Date of course:} & {02. - 06.03.2021}                                                  \\
				    \textbf{Supervisor:} & Joachim Köser, Theodor Bühler, Meikel Eichmann
			\end{tabular}
		}
		\bfseries
		
	\end{center}
	
	\vskip 2cm
		
		
\end{titlepage}

\tableofcontents

\newpage

\section{Introduction}

Given the current situation with a global pandemic affecting our everyday lives, rapid testing is emerging as a good way to keep the virus in check without the need for lockdowns. With key figures like Bill Gates backing this strategy the importance of point-of-care (POC) diagnostics has increased even further \cite{feuerBillGatesSays2020}. 

As such, the lateral flow assay (LFA) provides a rapid, cheap, easy to use and portable platform for the simultaneous detection and (semi-) quantification of multiple analytes in complex mixtures \cite{koczulaLateralFlowAssays2016}. It combines the unique advantages of biorecognition probes and chromatography \cite{sajidDesignsFormatsApplications2015}. However it suffers from the low affinity of biomolecules towards analytes and has a tendency towards cross reactivity, which, in combination with the broad availability, secures it a place as a frontline test that can indicate towards the existence of an analyte but still is not comparable to a fully equipped lab \cite{sajidDesignsFormatsApplications2015}.

To demonstrate just how easy to produce such LFAs are we will be producing and optimizing one ourselves.The goal of this report will be to discuss the production steps necessary for a simple LFA and the optimization steps thereafter. A quick glimpse into current development will be taken in the form of an LFA for the detection of covid receptor binding domain antibodies.


\section{Materials \& Methods}

Colloidal gold nanoparticles were coupled to antibodies to act as a visual indicator for the lateral flow assay. 

	\subsection{Gold Nanoparticle (NP) Synthesis}

	First a solution of 0.5 ml 1\% aq. gold chloride in 40 ml of Milli-Q purified water was prepared and heated to 60 \degs C.
	A second solution of 2 ml 1\% tri-sodium citrate (2x \ch{H20}) in 8 ml Milli-Q was also preheated to 60 \degs C. The two solutions were then mixed and heated to 100 \degs C under reflux while stirring for 5 minutes. After an observed colour change from clear to a dark red (see figure \ref{fig:gold_np_sol}) it was cooled on ice and the pH was set to around 8.5 using 0.5 M \ch{NaOH}. 
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.55\linewidth]{"data/IMG_5888"}
		\caption{Gold NP solution cooling in an ice bath after turning dark red.}
		\label{fig:gold_np_sol}
	\end{figure}
	
	Through characterization via UV-Vis absorption and Scanning Electron Microscopy (SEM) the diameter of the colloidal gold NPs was determined to be around 20-30 nm.
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.55\linewidth]{"graphs/GNP on Si_09"}
		\caption{SEM image of gold NPs on a Si wafer. Diameter of one measured NP at 26.24 nm.}
		\label{fig:gnp-on-si09}
	\end{figure}

	\subsection{Gold NP Antibody Coupling}
	
	To reduce antibodies waste the lowest antibody:gold ratio that stabilizes the NPs is determined though a dilution series of 5 different concentrations of FITC-IgG and one control. It was conducted with concentrations 50, 25, 12.5, 6.25, 3.125 and 0 $\mu$g/ml. 10 $\mu$l of each solution were added to 10 $\mu$l of gold NP solution and were left to incubate for 10 minutes. 
	\newpage
	After adding 20 $\mu$l of 10\% NaCl the non stabilizing concentrations tinted blue. Though visual confirmation we determined 50 and 25 $\mu$g/ml to be stabilizing. In order to proceed with the LFA we prepared a large batch of each concentration and a control group (pure Milli-Q, no antibodies). 
	200 $\mu$l of 50, 25 and 0 $\mu$g/ml FITC-IgG were mixed with 200 $\mu$l of gold NP solution and left to incubate for 60 minutes. 
	
	Finally the excess antibodies were blocked using 18 $\mu$l of 10\% casein solution (16 $\mu$l for the control). For resuspension the pellets were centrifuged at 7000 rpm for 30 minutes and then added to 100 $\mu$l of 10\% casein and 0.1\% Tween20 in a 10 mM phosphate buffer solution (PBS).
	
	\subsection{Lateral Flow Assay (LFA)}
	
	The LFA was comprised of four main parts: Plastic backed nitrocellulose acting as the stationary phase, a cellulose pad to absorb the liquid mobile phase and accelerate the flow, the immobilized testing substance and the resuspended gold NP solution as the mobile phase and analyte. This is visualized in figure \ref{fig:assetstream}, except that our analyte was already the conjugate and that there was no control line or sample pad.
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\linewidth]{graphs/AssetStream}
		\caption{Schematic of a standard LFA \cite{LateralFlowAssay}.}
		\label{fig:assetstream}
	\end{figure}
	
	After preparation of the assay 20 $\mu$l of blocking solution (10\% casein, 0.1\% Tween20) was applied to minimize non-specific binding of the antibody-gold complexes. 
	
		\subsubsection{Protein A LFA}
		
		For part 1 of the LFA the immobilized testing substance was 1 $\mu$l of Protein A spotted onto the nitrocellulose test strip and left to dry before blocking. For each of the following Assays 50 ml of gold-blocker mix was run though at varying ratios and using the differently stabilized gold NPs. To do so, the mixes were prepared in a 96 well plate and the LFA strips placed in each well. Following the 25 $\mu$g/ml IgG conjugated gold NP solution will be called solution 25 and the 50 $\mu$g/ml solution 50.
		\newpage
		For a functionality test we mixed a 1:4 ratio of gold:blocker for solutions 25, 50 and the control. Then we proceeded with a 2:3 ratio and a 1:9 ratio for both solutions 25 and 50. To test the limits of what is detectable we did 1:49 and 3:47 ratios on solution 25. Finally we tested variations such as 2 blots of protein A and a 1:10 dilution of the protein A solution using solution 25.
		
		\subsubsection{Covid Receptor Binding Domain (RBD) LFA}
		
		For part 2 of the LFA a solution of 4.5 $\mu$l RBD in 50 $\mu$l Milli-Q was prepared and added to 150 $\mu$l of the gold NP solution. After a short incubation period the solution was centrifuged and the beads re-suspended in 100 $\mu$l covid test running buffer (CTRB). On a nitrocellulose strip first a band of 1 $\mu$g human IgG and then a band of 1 $\mu$g protein A were spotted and the strip blocked using 20 $\mu$l of CTRB.
		
		Then 3 sample solutions were prepared: i) anti-RBD in 50 $\mu$l CTRB with 10 $\mu$l of RBD-coated NPs, ii) anti-RBD in 50 $\mu$l of human serum with 10 $\mu$l of RBD-coated NPs (antibody control) and finally iii) 10 $\mu$l of RBD-coated NPs in 50 $\mu$l CTRB (antigen control).
		
		All three were run on a separate LFA and compared to a stock LFA.
	
\section{Results and Discussion}

The first test with a ratio of 1:4 gold:blocker yielded the results we expected: No signal for the control and decent signal for both solutions 25 and 50 (see figure \ref{fig:test}). 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.55\linewidth]{data/IMG_20210209_161720_1}
	\caption{(ltr) Nitrocellulose strips run with gold NPs stabilized in 0 (control), 25 and 50 $\mu$g/ml IgG. Red dots confirm binding of the antibody-gold complex to the immobilized protein A}
	\label{fig:test}
\end{figure}

To further explore the signal strength of different solutions we then tested ratios of 2:3 and 1:9 for both solutions (see figure \ref{fig:variation1}).

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{data/IMG_20210209_162040_1}
	\caption{(ltr) LFA for solution 25, ratios 2:3 and 1:9 as well as for solution 50, ratios 2:3 and 1:9.}
	\label{fig:variation1}
\end{figure}

While the signal strength for solution 25 ratio 2:3 did not increase remarkably there is a noteworthy discoloration of the cellulose sponge indicating an excess of NPs. The signal for solution 50 ratio 2:3 showed neither a remarkable increase nor a discoloration of the sponge which could be due to the rather large size of the blotted protein A spot. Both solutions still showed a clear signal for the ratio 1:9 which led us to experiment further and test the boundaries what was still detectable. Since there was no clear difference in signal strength between solution 25 and 50 we continued our further experiments with solution 25 as it was more resource efficient to produce.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{data/IMG_20210209_163544_1}
	\caption{(ltr) LFA for solution 25 ratio 1:49, 3:47. Additionally solution 25 ratio 1:9 with two blots of protein A and a 1:4 dilution of the blotted protein A.}
	\label{fig:variation2}
\end{figure}

Though the signal for the 1:49 ratio was still visible, it was only barely so. The ratio 3:47 however yielded a good result at a low cost. Blotting two protein A spots confirmed that for the 1:9 ratio there was still excess NPs after one spot. Blotting more strongly diluted protein A (1:4 dilution) still showed a clear signal.

As for the Covid RBD LFA, the results were as expected: test i) showed as positive, ii) as positive too and iii) as negative. The stock LFA showed a negative result for our colleague Timon Flathmann who was kind enough to donate a few drops of his blood.

\section{Conclusion}

As seen in the results our production of LFAs seems to have been a full success. From forming the colloidal gold NPs to coupling them with antibodies. A step towards optimization was also taken and several ways to reduce resource consumption found. Given the ease of use as well as the cheap and rapid development it is of no surprise that LFAs represent a powerful tool in point-of-care diagnostics. Overall we learned a lot about immunoassays and their development as well as optimization.
	
\section{Acknowledgments}
I would like to thank Dr. Joachim Köser as well as the Swiss Nanoscience Institute for making this project possible. Further I would like to thank Theodor Bühler, Meikel Eichmann, Dr. Lucy Kind and Lena Mungenast for supervising us during these various experiments.


\bibliographystyle{IEEETRAN}
\bibliography{Bibliography}
	
\end{document}